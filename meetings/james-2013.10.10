G================
Unasked questions
=================
Why should we expect orthologs to follow from running InParanoid?
  We'd expect to see most ortholog groups composed of only one gene, I suppose
    Cases where we see ortholog groups -- are these just ones in which n-to-1 relationship appears for gene in both genomes?
How exactly do I assess biological function?
  Ideas:
    Each used different mRNA samples, different gene databases, presumably

AED: is it not a measure for tracking annotation differences from release to release?
  Went from 27k genes in Gasser paper, to 23.6k
In Gilleard paper, why was predicted genome size only 60 Mb?
  
If CEG missing in both, look at other non-Caenhaborditis nematodes
  Most likely difficult to assembly 

Any particular figures/tables that James would like to see?

=============
Research plan
=============
eypotheses: two published reference genomes for Haemonchus differ in how well they represent organism
  Hypothesis 1:
    Weak version: These are due to differences beyond biological -- are technological artifacts. We can determine these differences.
    Strong version: Majority of differences are errors in sequencing/assembly
      I prefer this

  Hypothesis 2:
    What are functions of genes that have been properly duplicated? (not technological artifacts)
      We suppose that "true" duplications are for genes involved with environmental response
        Involved in parasite survival -- immunomodulization of host
        True duplications are enriched for parasite survival

Aims: list each aim and its method

==========
Background
==========
Haemonchus is economically important parasite
  Widely used in drug discovery & vaccine development
  Surprising propensity to develop anthelmintic resistance
    Position in phylogenetic drug makes it prime species to compare against C. elegans
  Brief explanation of life cycle and etiology

Haemonchus is multicellular, genoms is x nt big
Talk briefly about how organism causes disease in host
Say: it eludes host immune detection -- it downregulates host response to worms
  We don't know how yet

My work: figure out what quality issues are
  We can't detect selection in two strains, but we can in 20
    Thus, this comparison of two lays groundwork for comparing twenty
    We're trying to figure out what's going on in two distinct strains
  Two types of selection: selection at sequence level, and at selection in gene family size -- expansion/contraction of gene families
Laing strain is African-derived, other's Australian via Europe
  So, there's relatively large amount of difference between them
  In comparing twenty strains, these would be the "goal posts" -- rather distant from each other

====
Aims
====
Tell what I'm going to use -- don't list parameters of program
  What I'm doing: Heuristic/phylogenetic approach both within and between genome similarity
  Perhaps use figure from InParanoid paper -- show how it works
Concisely:
  Aim 1: look at assembly
  Aim 2: assuming assembly is good, look at annotation

Need:
  Interpretation -- what is success/failure?
    What other explanations possible?
  Anticipated results
  Difficulties

----------------------------------------------------
1. Assess genome assembly differences for Haemonchus
----------------------------------------------------
WGA -- look at synteny
  
How do I take intronic divergence into account?
  Generally use protein sequences rather than nucleotide
Explain REAPR
  What genes are in poorly assembled regions? Likely ones that are hard to assemble -- cassettes of similar genes

---------------------------------------------
2. Examine structural annotation between both
---------------------------------------------
  Primarily use InParanoid for this

  How much overlap is there in gene sets?
    Obviously, at least 1800 genes in one and not other
      What is functional annotation for these genes?
      Can I find them in other organisms?

  If we see duplication, are the duplicated genes directly next to each other?
    We expect this to most common duplication -- tandem duplication
    Are there signs of retrotransposition? Less common
      Signs of retrotranspositions:
        Does the gene have a poly-A tail?
        Missing introns -- we should have introns
        Do we have flanking regions that look like retrotransposon sequences?

      Are these alleles that have been incorrectly treated as separate genes?
        They'd be very similar in sequence
        If one allele ends up off by itself with no other genes on small contig, this is sign of possible problem
          Maybe it's allele of a properly assembled gene
            Gene is too distinct to be collapsed, but no supporting evidence to place it alongside other genes -- so ends up off by itself
    How can we tell data is biologically good?
      If duplicated genes on same scaffold, then it's probably real
      If on different scaffolds, then it's more of a problem

    Must also compare Laing to Laing -- BLAST a whole contig against its genome
      Otherwise, instead of contigs, maybe BLAST each gene against the genome
      If we see gene by itself on contig, bearing similarity to larger contig, then perhaps this was misassembled allele

  Look at expression differences -- how do constitutivly expressed genes vary in SNV content between genomes, relative to genes dependant on lifecycle stage
    1k (or maybe 3k? James was unclear) cases of gene CNV where orthologous groups are not 1:1 in InParanoid -- 1:n instead
      James' method: aligned top 100 contigs by size between the two
      Mutation rate between genomes was 1%

  Investigate function of gene families that have undergone expansion
    Are they involved in in immunomodulation, or are otherwise essential to organism viability?

  Not discussed in proposal:
    Expression differences
    Functions of gene families undergone expansion
      Talk about genes missing from one genome, or that have undergone expansion in one -- what is their function?

    If Schwarz has longer gene than Laing, longer is likely correct
    BLAST the Schwarz against C. elegans -- do we find that full DNA seq is present?
      If so, this says Schwarz is likely right
    Can't really use annotated gene data sources from one to determine if other annotation is correct
      i.e., my idea is to take the transcripts in A, then see how they compare to annotations in B
        But which one is correct, then? We don't know
  
-------------------------------------------------
3. Compare against common set of eukaryotic genes
-------------------------------------------------
  73%/92% of partial/complete in Gasser
  90/93% of partial/complete in Gilleard

  CEG comparison: compare intron lengths for all genes, percent identity
    But we expect that differences in introns will be real -- if we find same complete CEG in both genomes, we suspect it's good
  We know CEGs are correct; should only find one copy -- this is good conservative first pass
    For a given gene, is it in same syntenic block across both genomes?
    Between the two genomes, for same CEG, how often do we find SNVs?
    This is fairly basic, but we haven't really done it before -- this is novel; we're only starting to do it now in C. elegans, which has 14 isolated strains
    Only characterized version thus is microsatellites

  In Scwarz: CEGs that are partial -- are they full in Laing? (and vice versa)
    Where's the rest of the gene in Schwarz?
      Is it on different/orphan contig?

  When you have a partial hit, how is it partial?
    Do you have 5% of the gene? 20%?

=================
Future directions
=================
Functional analysses
  Functionally analyze missing/duplicated -- are they critical to organism?
    Wetlab verification is ideal
  We generate hypotheses about important CNVs expressions -- use RNAi, expression profiles, proteomics
    Experimentally (in wetlab) validate our predictions
Look at wider range of Haemonchus strains -- examine gene selection
Apply analysis to Ascaris


=======
Outline
=======
Research plan: 1 page
  Summary
    Introduce field and its importance
      1/2 page summary of existing knowledge and my research plan
  Hypothesis
  Aims

Background/rationale: 2 pages
  Problem
  Current state of knowledge
  My plan
  How my plan will contribute to field

Aims:
  How will I conduct experiment?
  What are controls?
  How will I interpret data?
  What alternative explanations possible?

Future directions: 1/4 page

Significance: 1/4 page
  Shouldn't this be addressed in research plan?


==============
Figures/tables
==============
Table: Compare key metrics between two genomes (# genes, intron size, etc.)
Figure: Give example of InParanoid
Figure: Give example of how we compare CEGs
Figure: Example of gene misassembly, as per SURE presentation
  Perhaps show instead how alleles can be miasassembled
    Ideally, alleles should be collapsed -- when not, this is misassembly


==========
Miscellany
==========
Genomes:
  Schwarz: McMaster (MCM) genome
  Laing: MHCo3 (or something)

InParanoid written for old Blastp -- not Blast+
  Faster to do the BLASTs yourself, as you can run them in parallel
  But data needs some manual munging -- James can show how

InParanoid outgroup purpose: for given gene, if hit to outgroup is stronger than to inparalog, then we stop -- we remove it from the orthologous group
  This breaks up large clusters of orthologous genes into smaller ones of stronger orthology

How will I find enriched terms between enriched groups of CNVs?
  Gotcha and others will help -- goes up each level in GO hierarchy, where there's significant enrichment between two groups (or something)

Presentation date: James strongly prefers if I present on April 11, as April 4th is Constance's birthday

Proper spelling: inparalogue
Write proposal for only Haemonchs
  Don't need to mention Ascaris in proposal -- mention other Haemonchus strains
AED: James doesn't know what it was
  I may need to ask authors why AED < 0.4 used in Schwarz paper

What species should I use for CEG comparison?
  Dave's already run variety of nematode genomes through CEG
  I can say we'd compare to other nematodes (Clade V? other WormBase species?) for CEG stuff -- perhaps don't need to be specific as to species
  Are the genes poorly represented in CEG for Haemonchus (and maybe others) difficult to assemble?
    Are they poorly assembled according to REAPR?
  Look at non-Caenorhabiditis nematodes -- we know all CEGs are present in C. elegans

My presentation on Nov. 8:
  Target biomeds -- HSOCs may find it incomprehensible
  Present to people in this lab the week before -- but give them lots of notice of when I plan to present to them

Start with CEG analysis -- it's known-good reference of genes that should appear only once

In this lab, we use the term "synteny" -- no such thing as "positional orthology"
