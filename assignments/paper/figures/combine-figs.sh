#!/bin/bash
for foo in fig_*.svg; do
  inkscape --export-pdf $foo.pdf $foo
done

pdftk fig_*pdf cat output figures.pdf
rm fig_*.pdf
