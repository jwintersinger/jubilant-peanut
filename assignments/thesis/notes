Do I need an abstract section? An acknowledgements section?

Sections:
  Introduction
    Review literature
  Rationale
    Explain objectives/hypothesis
  Methods
    Sufficient detail
    Techniques clealry described
    Analytical/statistical/qualitative techniques described
  Results (1300 words)
    Positive/negative otucomes included
    Data presented in logical sequence
    Figures can be understood without reference to text
  Discussion (2900 words)
    Results interpreted, meaning/significance outlined
    Study placed in context with literature
    Negative outcomes discussed
    Avenues for future research indicated
  References

Extra sections:
  Conclusion (400 words)

Twenty pages, excluding figures, elgends, references, appendices

Figures/tables:
  Table showing stats from both papers
  CEG Venn diagrams
    Present complete CEGs
    Absent complete CEGs
    Four-way Venn diagram
    Visualization via Kablammo
  Syntenic map -- negative result

  Pie charts showing orthologous groups
    MHco3 / McMaster
    C. briggsae / C. elegans
    MHco3 / C. elegans
    McMaster / C. elegans
  Distribution of orthologous groups
  Number of scaffolds with multiple members of single orthologous group
  Structure of MHco3 orthologous gruops
  Distribution of orthologues (1:1, ...)
  Genome browser orthologue visualization
    2:1 orthologous group from MH -- intervening middle gene
    4:1 group orthologous gruop in MHco3 -- less likely to be valid
      Where is the one gene in McMaster? On its own scaffold?

Possible figures:
  Flow chart showing orthologous groups workflow
  Improvements to Kablammo

========
Abstract (100 words)
========

============
Introduction (600 words)
============
Available genome sequence data exploded in decade after publication of human genome {PRO1}
  Sequencing and assembly is a technically complex process {PRO3}, with both suffering from any number of potential problems
  Too much faith given to veracity of data
    This uncertainty is poorly represented in downstream genome {PRO2}

Assembly problems: Cannot resolve structure of repetitive sequences given short reads {PRO4}
  Hence, these are collapsed
  Distinguishing between alleles is hard
    All the more difficult given homogenized worms

Annotation problems: no less vexing -- also complicated {PRO5}
  Invalid annotations may propagate {PRO6}
  Ab initio gene prediction accuracy approaches 100% {PRO5}, but ability to accurately denote intron/exon boundaries is only 60-70% {PRO5}
    This only works given numerous high-quality gene models specific to organism in question {PRO5}, which are typically unavailable for newly sequenced species
  RNA-seq can only capture expressed genes -- these may change according to lifecycle or environmental conditions



Minimnal other studies done

Past studies comparing annotations, whether by inspecting multiple annotations for one species produced by automated pipelines {PRO7} or developing methods to contrast different annotations with one another {PRO8,9}, have applied only to different annotations produced for a single assembly. My work will be novel in that it must also account for assembly variation, introducing significant complexity—reannotating an improved Bos taurus assembly yielded substantial changes in 40% of genes, with 660 protein-coding genes lost in the improved version {PRO10}.

Strains:
  I plan to catalogue discrepancies in genome assembly and annotation between two recently published draft genomes for the parasitic nematode Haemonchus contortus. One corresponds to a laboratory strain derived from an African Haemonchus sample (herein referred to as the MHco3(ISE) genome) {PRO11), and the other to an Australian field isolate (herein referred to as the McMaster strain) {PRO12}.
    What is evolutionary distance of these?


=========
Rationale (400 words)
=========
Objective: compare two recently published genomes
  Catalogue discrepancies between genomes, infer which result from technical error, which from biological variation
    Focus on assembly and annotations
      Note that errors in former will propagate to latter

Clear difference between genomes: see table
  Size of assembly
  Number of genes
  Scaffold N50
These differences are far greater than we would expect given legitimate biological variation

Hypothesis: minimal variation will result
  Largely limited to small-scale duplication/deletion of non-critical genes
  What variation remains will largely be result of technical error
  MHco3 will be higher quality -- more homogeneity, so easier to assemble & annotate

Haemonchus presents unusual opportunity:
  Two genomes published concurrently
  Both use state-of-the-art sequencing, assembly, and annotation methods
    Both done as collaborative projects amongst parties possessing considerable experience in field

Haemonchus:
  Haemonchus' suitability arises not merely from the recent publication of two genomes for it. Haemonchus contortus is an economically important parasites that has infected hundreds of millions of sheep and goats globally {PRO13,14}, producing losses of tens of billions of dollars per annum. Feeding on capillary blood in stomach mucosa {PRO14}, Haemonchus causes anemia and hemorrhagic gastritis in infected animals. A single female parasite can produce up to 10,000 eggs per day {PRO15}, while an infected animal may bear thousands of worms.

Though Haemonchus infections are commonly treated via anthelmintic drugs, the worm has demonstrated the ability to rapidly develop resistance {PRO16}. As in vivo Haemonchus studies can be undertaken in the parasite's natural host, including genetic crosses {PRO17}, Haemonchus is as an ideal model for strongylid drug resistance and vaccine development. With the organism exhibiting substantial genomic diversity between strains due to its large effective population size {PRO18,19}, it is suitable for my proposed study, which endeavors to distinguish technical errors despite biological variation.

=======
Methods (600 words)
=======
All scripts used in workflow, as well as code for novel methods, published at {url}
Data sets:
  MHco3: PRJEB506 in WormBase WS239
  McMaster: PRJNA205202 in WormBase WS239

CEGMA:
  Versions:
    BLAST 2.2.27+
    CEGMA 2.4.010312
    Kablammo (written by authors): Feb 23, 2014
  Ran CEGMA to determine presence of 248 most highly conserved CEGs
    Patched it to output names of complete/partial CEGs
  In examining misassembly (FIGkablammo) instances, CEGs in one genome were BLASTed against other assembly
    For each complete/partial gene, the coding sequences were extracted from the original genomes, then BLASTed against other
    Results then visualized via Kablammo

Orthology
  Versions:
    InParanoid 4.1 using BLAST 2.2.6
  Ran with BLOSUM{62,80} -- settled on altter given expect similarity
  Comparisons:
    For Ha/Ha, used PRJNA13758 WS239 (C. elegans) as outgroup -- but also tried without outgorup
    For Ce/Cb, used BLOSUM62/80 with no outgroup
    For Ha/Ce, used BLOSUM62/80 with no outgroup
  In Haemonchus datasets, filtered isoforms before running InParanoid
    Isoforms filtered based on FASTA IDs indicating that multiple annotated sequences corresponded to same genes
    Lack of similar convention for indicating isoform in Ce/Cb meant we didn't filter these
  State-machine-based parser written to parse InParanoid output {ref}
  Scaffold on which each orthologue then determined
  Structure of orthologous groups visualized using online GBrowse instance provided by WormBase on Mar 3, 2014
    FIGindivortho4to1
    FIGindivortho2to1

Syntenic plots
  Versions:
    MUMmer 3.23
    LAST 393
    Sibelia c371c601
    SyMAP 4.0
    Mauve 2.3.1
    SynMap as of Feb. 4, 2014
  SynMap ran against the two Haemonchus assemblies/annotations using default parameters
    i.e.:
      LAST alignment algorithm
      Ran DAGChainer with "relative gene order" option
      Used CDS rather than whole genomic sequence
      Did not use algorithm for "merge syntenic blocks" or "syntenic depth" options


==========
Conclusion (100 words)
==========
Poor conservation of CEGs in both genomes
  Surprising lack of overlap between genomes in which CEGs are represented -- suggests each fared better with regard to some CEGs than others
  Clear evidence of misassembly when visualizing fragments of missing CEGs in each
Far fewer genes in ortho groups between genomes than expected -- only 40% and 30%
  Compare to only 78 and 102 unique genes found in A. oryzae {Zhao 2013}, comprising less than 1% of genes in each
  Nearly twice as many genes in MHco3 found to be orthologus to C. elegans than McMaster
    MHco3 value likely to be closer to true orthologous relationship with C. elegans
  MH contained many more orthologues on the whole
  Complete lack of synteny

Conclusion:
  Somewhat better CEG representation, as well as better orthologous relationship to C. elegans, suggests MH is better
    Number of quality issues remain, however

Future directions:
  Figure out how to better compute synteny
  Find missing orthologus
  PCR to verify instances of tandem duplications
  Expand to more strains
    Compare Ascaris genomes


=======
Results
=======
--------------------------------------------
Numerous CEGs are missing from each assembly
--------------------------------------------
Numerous CEGs are missing
  We expect ~1 to be missing -- list drawn from H. sapeins, C. elegans, Arabidopsis, etc.
Couldn't repeat authors' results
MH generally better than Mc
  Fewer complete CEGs missing
  Fewer complet/epartial CEGs missing
  Great discrepancy in partial vs. complete
But MH still isn't great -- significant things are missing

----------------------------------------------
Missing CEGs show clear evidence of miassembly
----------------------------------------------
Example: middle portion of gene missing
Some exons assembled out of order
  Possible duplications -- include figure of mousing over

------------------------------------------------
Many gene models lack orthologues across genomes
------------------------------------------------
Ideally, we'd have 1:1 correspondence of Haemonchus genes
  This won't quite happen, given biological discrepancies
But we find only 40%/30% of genes in orthologous groups
  Fundamental pipeline is sound -- compared against C. elegans, where we replicated results of previous study
Finally, did Haemonchus/C.elegans comparison
  Result: 49% MH, 26% McMaster
  Suggests significantly better gene models in MH
    Would be fruitful to repeat with less stringent parameters

This is confounding -- two organisms that diverged 100 (20?) MYA possess more similarity to each other than do two strains of same organism

------------------------------------------------------------
Contributions to orthologous groups vary between the genomes
------------------------------------------------------------
33.5% of orthologous groups are 1:n or n:1
  These may reflect legitimate duplications, or technical error
Large discrepancy between MHco3 and McMaster
  In general, groups contain many more MH genes than McMaster genes
  Could be good for MH: means better job done of resolving highly similar genes into discrete loci
    Or could be bad for MH: dealt poorly with slightly different alleles
      This is particularly problematic, given that we're sequencing from pooled sample, with significant interorganism diversity (ref?)

---------------------------------------------------
MH has many more homologues placed on same scaffold
---------------------------------------------------
110 instances of multiple genes on same scaffold for MH
  Only three for McMaster
These are generally indicative of better assembly
  When struggling to assemble alleles, if we can't resolve proper structure, we're more likely to shuttle each allele off onto its own scaffold
  Presenece of other genes (18 and 71 instances) further validates these scaffolds

Instances of duplication -- either as inparalogues or outparalogues -- often expected to result in tandem arrangement
  Thus, lack of these instances in McMaster is concerning
Nevertheless, placement of orthologues on different scaffolds need not be damning -- show 4:1 orthologous group in which you have radically different gene structure for at least three members
  
==========
Discussion
==========
----
CEGs
----
MH generally better, but neither genome great
  We expect to see almost all CEGs
Striking are differences between genomes
  Complete vs. partial (2/33): big discrepancy
Repeatability also a concern

Out-of-order assembly, and duplications of CDS portoins, complicate CEG finding -- evidence of miassembly

---------
Orthology
---------
We expect almost all genes to fall into 1:1 orthologous groups
  Relatively few inparalogues should exist -- these would be legitimate duplications after strains diverged from LCA
  We may find any number of outparalogues, which would be orthologues to each other
  Cases where a given gene isn't in an orthologous group are instances of deletion, or rapid mutation of a gene

Our results is far below this expected value -- we'd expect upwards of 95% genes would be in orthologous groups, most of which would be 1:1
  We see only 30% to 40% -- and only 63.9% of these are 1:1
Replicated C.elegans/briggsae result suggests our method is valid

When comparing C.elegans/Haemonchus, we see divergent results
  In MH: more genes orthologous to C. elegans than to McMaster
    In McMaster: fewer genes orthologous to C.elegans than MH
  This supports notion that gene models in McMaster are poor -- 49% is reasonable between MH/C. elegans, given 65% between C. elegans/briggsae
    26% of McMaster is too low
    Prime question going forward: what of the 49-26=23%?
      Are these amongst the genes found orthologous between the two Haemonchus genomes?
      What if we ran with relaxed BLAST parameters? This would compensate for poorer McMaster gene models in which, say, a single exon was often missing

MHco3, despite having 1800 fewer genes, has substantially more in orthologous groups -- 40% vs. 30%
  Likewise: size distribution skewed
    1:n and n:1 distributions aren't equal
    MH contributes, on average, many more genes
  This could be good or bad for MH
    Good interpretation: MH did better job of assembling highly similar genes
    Bad: MH did poor job of resolving alleles -- instead of deeming them to be copies of same gene that correspond to same physical locus, it deemed them separate genes

  May be able to decide good/bad based on structure
    If alleles resolved poorly, many copies should be off on same scaffold
      We do indeed see this for some -- but in given example, different gene structures suggest that at least three genes are valid

-------
Synteny
-------
Extremely concerning
  Determing widespread synteny between the genomes would be unremarkable
    Only when we find so little synteny is the result notable
Possible causes:
  Data really bad in one or both genomes
    Considerable number of missing CEGs hints at bad assembly
    Lack of orthology -- synteny relies entirely on gene models properly representing orthologous genes
      If this is bad, so too will be the synteny
  Heavily fragmented nature of genome
      
====
TODO
====
Better address study objectives in Rationale

Add figure showing moused-over Kablammo to make overlap clear
Remove titles from figurs
Standardize on one figure after decimal in percentages
