#!/bin/bash
set -euo pipefail
ROOT_DIR=~/work/jubilant-peanut
RESULTS_DIR=$ROOT_DIR/runs/locate-missing-orthologues

orthos_dir=$ROOT_DIR/runs/orthomcl/PRJEB506-PRJNA205202
ortho_results=$orthos_dir/results/orthomcl_results.json

function determine_missing_orthologues {
  ./determine-missing-orthologues.py \
    $ortho_results \
    'a' \
    $orthos_dir/proteins/PRJEB506.filtered_isoforms.fa \
    $ROOT_DIR/data/PRJEB506/h_contortus.PRJEB506.WS239.cds_transcripts.fa \
    $RESULTS_DIR/missing-proteins.PRJEB506.fa \
    $RESULTS_DIR/missing-transcripts.PRJEB506.fa

  ./determine-missing-orthologues.py \
    $ortho_results \
    'b' \
    $orthos_dir/proteins/PRJNA205202.filtered_isoforms.fa \
    $ROOT_DIR/data/PRJNA205202/h_contortus.PRJNA205202.WS239.cds_transcripts.fa \
    $RESULTS_DIR/missing-proteins.PRJNA205202.fa \
    $RESULTS_DIR/missing-transcripts.PRJNA205202.fa
}

function perform_blast {
  ./blast-missing-orthologues.sh \
    PRJEB506 \
    $RESULTS_DIR/missing-proteins.PRJNA205202.fa \
    $RESULTS_DIR/missing-transcripts.PRJNA205202.fa &

  ./blast-missing-orthologues.sh \
    PRJNA205202 \
    $RESULTS_DIR/missing-proteins.PRJEB506.fa \
    $RESULTS_DIR/missing-transcripts.PRJEB506.fa &

  wait
}

function plot_results {
  for results in $RESULTS_DIR/*.blast.xml; do
    base=$(echo $results | sed 's/.blast.xml$//')
    completeness_json=${base}.gene_completeness.json
    cat $results | ./calc-gene-completeness.py > $completeness_json
    cat $completeness_json | \
      ./filter-json-col.py proportion | \
      ./plot_histogram.py ${base}.gene_completeness.png
  done

  ./plot_histogram.py \
    $RESULTS_DIR/blastp_proportions_cmp.svg \
    <(cat $RESULTS_DIR/missing-proteins.PRJNA205202.blastp.gene_completeness.json | ./filter-json-col.py proportion) \
    <(cat $RESULTS_DIR/missing-proteins.PRJEB506.blastp.gene_completeness.json    | ./filter-json-col.py proportion)

  for genome in PRJEB506 PRJNA205202; do
     ./examine-missing-orthologues.py \
       $RESULTS_DIR/missing-proteins.${genome}.blastp.gene_completeness.json \
       $RESULTS_DIR/missing-proteins.${genome}.tblastn.gene_completeness.json \
       $RESULTS_DIR \
       $genome \
       > $RESULTS_DIR/missing_orthos.${genome}.stats
  done
}

function main {
  mkdir -p $RESULTS_DIR
  determine_missing_orthologues
  perform_blast
  plot_results
}

main
