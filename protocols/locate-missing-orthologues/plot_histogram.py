#!/usr/bin/env python3
import sys
import numpy as np

import matplotlib
# Force matplotlib not to use X11 backend, which produces exception when run
# over SSH.
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def plot_histogram(vals, graph_filename, xrange=(0, 1), bins=10):
  weights = np.ones_like(vals) / len(vals)
  plt.figure()
  counts, bins, patches = plt.hist(
    vals,
    log=False,
    facecolor='green',
    alpha=0.5,
    weights=weights,
    range=xrange,
    bins=bins,
  )
  plt.ylim([0, 1])
  plt.xlim(xrange)

  bin_centers = 0.5 * np.diff(bins) + bins[:-1]
  for count, x in zip(counts, bin_centers):
    plt.annotate(
      '%.3f' % count,
      xy=(x, 1),
      xycoords=('data', 'axes fraction'),
      xytext=(0, -18),
      textcoords='offset points',
      va='top',
      ha='center',
      rotation=45
    )

  plt.savefig(graph_filename)

def plot_twoway_histogram(vals_a, vals_b, graph_filename, xrange=(0, 1), bins=10):
  weights1 = np.ones_like(vals_a) / len(vals_a)
  weights2 = np.ones_like(vals_b) / len(vals_b)

  comphist(
    vals_a,
    vals_b,
    weights1=weights1,
    weights2=weights2,
    orientation='vertical',
    log=False,
    facecolor1='#491b3a',
    facecolor2='#1b4349',
    edgecolor='white',
    range=xrange,
    bins=bins,
  )
  plt.savefig(graph_filename)

def comphist(x1, x2, orientation='vertical', **kwargs):
  """Draw a comparative histogram."""
  # Taken from http://log.ooz.ie/2013/02/matplotlib-comparative-histogram-recipe.html.

  # Split keyword args:
  kwargs1 = {}
  kwargs2 = {}
  kwcommon = {}
  for arg in kwargs:
    tgt_arg = arg[:-1]
    if arg.endswith('1'):
      arg_dict = kwargs1
    elif arg.endswith('2'):
      arg_dict = kwargs2
    else:
      arg_dict = kwcommon
      tgt_arg = arg
    arg_dict[tgt_arg] = kwargs[arg]
  kwargs1.update(kwcommon)
  kwargs2.update(kwcommon)

  matplotlib.rc('font', size=10)
  fig = plt.figure(figsize=(5, 4))

  # Have both histograms share one axis.
  if orientation == 'vertical':
    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212, sharex=ax1)

    ax1.set_ylim([0, 0.5])
    ax2.set_ylim([0.5, 0])

    # Flip the ax2 histogarm horizontally.
    plt.setp(ax1.get_xticklabels(), visible=False)
    legend_loc = (1, 4)

  for ax in (ax1, ax2):
    ax.grid(True, linestyle='-', color='white', linewidth=0.5)
    for spine in ('top', 'bottom', 'right', 'left'):
      ax.spines[spine].set_color('white')#'#ababab')
    ax.tick_params(tick1On=False, tick2On=False)

  ax2.spines['top'].set_color('white')
  ax2.spines['bottom'].set_color('#9f9f9f')
  ax1.spines['left'].set_color('#9f9f9f')
  ax2.spines['left'].set_color('#9f9f9f')
  ax2.tick_params(axis='x', tick1On=True)

  ax1.hist(x1, orientation=orientation, **kwargs1)
  ax2.hist(x2, orientation=orientation, **kwargs2)
  ax1.legend(loc=legend_loc[0])
  ax2.legend(loc=legend_loc[1])
  # Tighten up the layout.
  plt.subplots_adjust(wspace=0.0, hspace=0.0)
  return fig

def fetch_vals(fd):
  vals = [float(l.strip()) for l in fd]
  return vals

def main():
  num_args = len(sys.argv[1:])
  graph_filename = sys.argv[1]

  if num_args == 1:
    vals = fetch_vals(sys.stdin)
    plot_histogram(vals, graph_filename)

  elif num_args == 3:
    val_filenames = {
      'a': sys.argv[2],
      'b': sys.argv[3],
    }
    vals = {}
    for set_name, fn in val_filenames.items():
      with open(fn) as fd:
        vals[set_name] = fetch_vals(fd)
    plot_twoway_histogram(vals['a'], vals['b'], graph_filename)

  else:
    print(sys.argv)
    raise Exception('Wrong number of args')

if __name__ == '__main__':
  main()
