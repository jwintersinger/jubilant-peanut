#!/usr/bin/env python3
from collections import OrderedDict
from plot_histogram import plot_histogram
import json
import os
import sys

def find_hits_with_completion(genes, lower_threshold, upper_threshold):
  return [
    g for g in genes
    if lower_threshold <= g['proportion'] <= upper_threshold
  ]

def find_hits_with_length_ratios(genes, lower_threshold, upper_threshold):
  return [
    g for g in genes
    if lower_threshold <= (g['query_len'] / g['subject_len']) <= upper_threshold
  ]

def calc_stats(query_set, comparison_set):
  comparison_set = index_by_query_def(comparison_set)
  values = OrderedDict()
  stats = OrderedDict()
  stats['total_genes'] = len(query_set)

  hq_gene_models = find_hits_with_completion(query_set, 0.9, 1.1)
  lq_gene_models = find_hits_with_completion(query_set, 0, 0.1)
  hq_with_similar_lengths = find_hits_with_length_ratios(hq_gene_models, 0.9, 1.1)
  hq_with_diff_lengths = find_hits_with_length_ratios(hq_gene_models, 0, 0.6)

  stats['hq_gene_models'] = len(hq_gene_models)
  stats['lq_gene_models'] = len(lq_gene_models)
  stats['hq_similar_lengths'] = len(hq_with_similar_lengths)
  stats['hq_diff_lengths'] = len(hq_with_diff_lengths)

  values['hq_length_ratios'] = [g['query_len'] / g['subject_len'] for g in hq_gene_models]
  values['hq_diff_lengths_tblastn_proportions'] = [
    comparison_set[g['query_def']]['proportion'] for g in hq_with_diff_lengths
  ]
  values['lq_tblastn_proportions'] = [
    comparison_set[g['query_def']]['proportion'] for g in lq_gene_models
  ]

  stats['lq_gene_models_hq_tblastn_hits'] = len([v for v in values['lq_tblastn_proportions'] if v >= 0.9])
  stats['lq_gene_models_lq_tblastn_hits'] = len([v for v in values['lq_tblastn_proportions'] if v <= 0.1])

  return stats, values

def index_by_query_def(gene_set):
  genes = {}
  for gene in gene_set:
    genes[gene['query_def']] = gene
  return genes

def main():
  blastp_fn = sys.argv[1]
  tblastn_fn = sys.argv[2]
  results_dir = sys.argv[3]
  genome_name = sys.argv[4]

  with open(blastp_fn) as f:
    blastp_set = json.load(f)
  with open(tblastn_fn) as f:
    tblastn_set = json.load(f)

  stats, values = calc_stats(blastp_set, tblastn_set)

  for key, val in stats.items():
    print('%s=%s' % (key, val))

  for key, vals in values.items():
    # Permit 1.0 values to be binned in [0.9, 1.0) bin.
    adjusted_vals = [v == 1.0 and v - 1e-10 or v for v in vals]
    out_prefix = os.path.join(results_dir, '%s.%s' % (key, genome_name))
    plot_histogram(adjusted_vals, out_prefix + '.png', xrange=(0, 1.1), bins=11)
    with open(out_prefix + '.vals', 'w') as f:
      f.write('\n'.join([str(v) for v in vals]) + '\n')

main()
