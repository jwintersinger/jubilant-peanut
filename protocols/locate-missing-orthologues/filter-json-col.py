#!/usr/bin/env python3
import json
import sys

def main():
  json_in = json.load(sys.stdin)
  col_name = sys.argv[1]

  for record in json_in:
    print(record[col_name])

main()
