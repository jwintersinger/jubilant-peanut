import xml.etree.ElementTree as ET
from collections import namedtuple, defaultdict

Query = namedtuple('Query', [
  'query_def',
  'query_length',
  'hits',
])

Hit = namedtuple('Hit', [
  'subject_length',
  'subject_def',
  'hsps',
])

Hsp = namedtuple('Hsp', [
  'query_start',
  'query_end',
  'query_frame',
  'subject_start',
  'subject_end',
  'subject_frame',
  'alignment_length',
  'bitscore',
])

class BlastParser(object):
  def _determine_strand(self, frame):
    if frame > 0:
      return '+'
    elif frame < 0:
      return '-'
    else:
      return '.'

  def parse(self, blast_xml):
    tree = ET.parse(blast_xml)
    root = tree.getroot()
    queries = []

    for iteration_elem in root.find('BlastOutput_iterations').iter('Iteration'):
      query = Query(
        query_length = int(iteration_elem.find('Iteration_query-len').text),
        query_def    = iteration_elem.find('Iteration_query-def').text,
        hits         = []
      )

      hit_elems = iteration_elem.find('Iteration_hits')
      if hit_elems is None:
        continue
      for hit_elem in hit_elems.iter('Hit'):
        hit = Hit(
          subject_length = int(hit_elem.find('Hit_len').text),
          subject_def    = hit_elem.find('Hit_def').text,
          hsps           = defaultdict(lambda: []),
        )

        for hsp_elem in hit_elem.find('Hit_hsps').iter('Hsp'):
          hsp = Hsp(
            query_frame = int(hsp_elem.find('Hsp_query-frame').text),
            subject_frame = int(hsp_elem.find('Hsp_hit-frame').text),
            alignment_length = int(hsp_elem.find('Hsp_align-len').text),
            query_start = int(hsp_elem.find('Hsp_query-from').text),
            query_end = int(hsp_elem.find('Hsp_query-to').text),
            subject_start = int(hsp_elem.find('Hsp_hit-from').text),
            subject_end = int(hsp_elem.find('Hsp_hit-to').text),
            bitscore = float(hsp_elem.find('Hsp_bit-score').text),
          )

          # Ensure sequence start coordinate is always less than sequence end
          # coordinate (which they won't be for sequences on minus strand).
          for seq_type in ('query', 'subject'):
            if getattr(hsp, '%s_frame' % seq_type) < 0:
              start_key = '%s_start' % seq_type
              end_key   = '%s_end' % seq_type
              start     = getattr(hsp, start_key)
              end       = getattr(hsp, end_key)

              if seq_type == 'query':
                length = query.query_length
              elif seq_type == 'subject':
                length = hit.subject_length

              if start > end:
                tmp = start
                start = end
                end = tmp

              hsp = hsp._replace(**{
                start_key: length - end + 1,
                end_key:   length - start + 1,
              })

          key = self._determine_strand(hsp.query_frame) + \
                self._determine_strand(hsp.subject_frame)
          hit.hsps[key].append(hsp)
        query.hits.append(hit)
      queries.append(query)
    return queries
