#!/bin/bash

function set_env {
  export BASEDIR=~/work/jubilant-peanut
}

# Not called by default, so call it manually if necessary
function make_protein_blast_dbs {
  cd $BASEDIR/data/blastdbs

  for foo in PRJEB506 PRJNA205202; do
    # Create temporary file without tabs in sequence IDs.
    tmp_fname=$(mktemp)

    cat $BASEDIR/data/$foo/h_contortus.$foo.WS239.protein.fa | sed 's/\t/ /g' > $tmp_fname
    makeblastdb -in $tmp_fname -dbtype prot -title ${foo}_WS239_protein -out ${foo}_WS239_protein > /dev/null
    rm $tmp_fname
  done
}

function main {
  set_env
  make_protein_blast_dbs
  blastp -query $missing_proteins -db $BASEDIR/data/blastdbs/${data_set}_WS239_protein -evalue 0.001 -outfmt 5 -out $(echo $missing_proteins    | sed 's/.fa$/.blastp.blast.xml/') -num_threads 20 &
  blastn -query $missing_transcripts -db $BASEDIR/data/blastdbs/${data_set}_WS239_genomic -evalue 0.001 -outfmt 5 -out $(echo $missing_transcripts | sed 's/.fa$/.blastn.blast.xml/') -num_threads 20 &
  tblastn -query $missing_proteins -db $BASEDIR/data/blastdbs/${data_set}_WS239_genomic -evalue 0.001 -outfmt 5 -out $(echo $missing_proteins | sed 's/.fa$/.tblastn.blast.xml/') -num_threads 20 &
  wait
}

data_set=$1
missing_proteins=$2
missing_transcripts=$3
main
