#!/usr/bin/env python3
'''
Write all transcript and protein sequences corresponding to proteins not in
ortho results.
'''
from dregs import binf
import json
import sys

# TODO: Convert this to use same method as in extra-seqs.py to prevent code
# duplication.
def write_missing_proteins(fasta_input_fname, fasta_output_fname, proteins_in_ortho_groups):
  with open(fasta_output_fname, 'w') as output_file:
    with open(fasta_input_fname) as input_file:
      for seq_id, seq in binf.parse_fasta(input_file):
        transcript_name = seq_id.split()[0]
        if transcript_name not in proteins_in_ortho_groups:
          binf.write_fasta_seq(output_file, seq_id, seq)

def main():
  ortho_results_fname = sys.argv[1]
  col_name = sys.argv[2]
  proteins_fasta_input_fname = sys.argv[3]
  transcripts_fasta_input_fname = sys.argv[4]
  missing_proteins_output_fname = sys.argv[5]
  missing_transcripts_output_fname = sys.argv[6]

  with open(ortho_results_fname) as ortho_results_file:
    ortho_results = json.load(ortho_results_file)

  proteins_in_ortho_groups = set()
  for group in ortho_results['groups']:
    for protein_name, score in group[col_name]:
      # Note that ortho results are based on data set with isoforoms
      # filtered. Thus, all isoforms of given gene will be deemed missing.
      proteins_in_ortho_groups.add(protein_name)

  write_missing_proteins(
    proteins_fasta_input_fname,
    missing_proteins_output_fname, 
    proteins_in_ortho_groups
  )
  write_missing_proteins(
    transcripts_fasta_input_fname,
    missing_transcripts_output_fname, 
    proteins_in_ortho_groups
  )

if __name__ == '__main__':
  main()
