#!/usr/bin/env python3

import sys
from dregs import binf

def main():
  discard_below = int(sys.argv[1])
  for header, seq in binf.parse_fasta(sys.stdin):
    if len(seq) >= discard_below:
      binf.write_fasta_seq(sys.stdout, header, seq)

if __name__ == '__main__':
  main()
