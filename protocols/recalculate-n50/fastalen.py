#!/usr/bin/env python3

import sys
from dregs import binf

def main():
  for header, seq in binf.parse_fasta(sys.stdin):
    print(len(seq))

if __name__ == '__main__':
  main()
