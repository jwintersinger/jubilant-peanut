DATA_DIR=~/work/jubilant-peanut/data
WS_VER=WS239
PRJNA205202=$DATA_DIR/PRJNA205202/h_contortus.PRJNA205202.${WS_VER}.genomic.fa
PRJEB506=$DATA_DIR/PRJEB506/h_contortus.PRJEB506.${WS_VER}.genomic.fa

# Find smallest scaffold in PRJNA205202. Result should be 201.
cat $PRJNA205202 | ./fastalen.py | sort -nr | uniq -c | tail -n 1
echo
DISCARD_THRESHOLD=201

echo "PRJEB506 original:"
cat $PRJEB506 | ~/work/dregs/dregs/bin/seqlenstats
echo

echo "PRJNA205202 original:"
cat $PRJNA205202 | ~/work/dregs/dregs/bin/seqlenstats
echo

echo "PRJEB506 with sequences shorter than $DISCARD_THRESHOLD discarded:"
cat $PRJEB506 | ./fasta-discard-below.py $DISCARD_THRESHOLD | ~/work/dregs/dregs/bin/seqlenstats
