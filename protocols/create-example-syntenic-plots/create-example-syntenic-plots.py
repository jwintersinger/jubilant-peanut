#!/usr/bin/env python2
import matplotlib
# Force matplotlib not to use X11 backend, which produces exception when run
# over SSH.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import string

def plot(A, B, title):
  plt.figure()
  plt.xticks(range(min(A), max(A) + 1))
  plt.yticks(range(min(B), max(B) + 1))
  plt.xlabel('Genome $\\alpha$')
  plt.ylabel('Genome $\\beta$')
  plt.title(title)
  plt.grid()
  plt.scatter(A, B, s=800, color='#085D82') # s=size

  labels = list(string.ascii_letters[:len(A)].upper())
  for i in range(len(A)):
    plt.annotate('%s' % labels[i], xy = (A[i], B[i]), ha='center', va='center', color='#FFFFFF')

  plt.savefig('%s.png' % title.lower())

def main():
  A = list(range(1,11))
  B = list(range(1,11))
  plot(A, B, 'Normal')

  B = [1, 2, 3, 8, 7, 6, 5, 4, 9, 10]
  plot(A, B, 'Inversion')

  B = [(b < 5 and b) or (b + 5) for b in range(1, 11)]
  plot(A, B, 'Insertion')

  B = [1, 2, 3, 5, 6, 7, 8, 9, 4, 10]
  plot(A, B, 'Translocation')

if __name__ == '__main__':
  main()
