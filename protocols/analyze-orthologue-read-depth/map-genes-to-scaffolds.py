#!/usr/bin/env python2
import argparse
import json
import HTSeq

def map_to_scaffold(gff_fn):
  gene_map = {}

  gff = HTSeq.GFF_Reader(gff_fn)
  for feature in gff:
    if feature.type.lower() != 'gene':
      continue
    gene_name = feature.attr['Name']
    gene_map[gene_name] = feature.iv.chrom

  return gene_map

def main():
  parser = argparse.ArgumentParser(description='LOL HI2U')
  parser.add_argument('gff_file')
  args = parser.parse_args()

  gene_map = map_to_scaffold(args.gff_file)
  print(json.dumps(gene_map))

main()
