#!/usr/bin/env python3
import argparse
import sys
import json
import numpy as np

def remove_outliers(vals):
  # Throw away top 1% of most extreme data from both ends of distribution.
  vals = sorted(vals)
  n = int(len(vals) * 0.01)
  vals = vals[n:-n]
  return vals

# Alternative to calc_1to1_avg_rd().
def calc_global_avg_rd(gene_rd):
  depths = {}
  for col in ('a', 'b'):
    dep = list(gene_rd[col].values())
    #dep = remove_outliers(dep)
    #depths[col] = np.mean(dep)
    depths[col] = np.median(dep)
  return depths

def calc_1to1_avg_rd(ortho_groups, gene_rd):
  depths_1to1 = {
    'a': [],
    'b': [],
  }
  for group in ortho_groups:
    if not (len(group['a']) == len(group['b']) == 1):
      continue
    for col in ('a', 'b'):
      gene_name = group[col][0][0]
      depths_1to1[col].append(gene_rd[col][gene_name])

  averages = {}
  for col, depths in depths_1to1.items():
    #depths = remove_outliers(depths)
    #averages[col] = np.mean(depths)
    averages[col] = np.median(depths)
  return averages

def calc_group_avg_rd(group, gene_rd):
  depths = []
  for gene_name, score in group:
    depths.append(gene_rd[gene_name])
  return np.mean(depths)

def calc_ratio_rel_to_global(group, expn_col, compn_col, indiv_gene_rd, global_gene_rd):
  compn_rd = calc_group_avg_rd(group[compn_col], indiv_gene_rd[compn_col])
  expn_rd = calc_group_avg_rd(group[expn_col], indiv_gene_rd[expn_col])

  compn_ratio = compn_rd / global_gene_rd[compn_col]
  expn_ratio = expn_rd / global_gene_rd[expn_col]

  return (compn_ratio, expn_ratio)

def calc_gene_scaf_rd_ratio(group, gene_to_scaffold_map, indiv_gene_rd, scaffold_rd):
  ratios = []
  for gene_name, score in group:
    gene_rd = indiv_gene_rd[gene_name]
    scaf_name = gene_to_scaffold_map[gene_name]
    # Append '_.' to follow convention for indicating unstrandedness in
    # scaffold RD file.
    scaf_rd = scaffold_rd['%s_.' % scaf_name]
    ratio = gene_rd / scaf_rd
    ratios.append(ratio)
  return np.mean(ratios)

def calc_ratio_rel_to_scaffold(group, expn_col, compn_col, indiv_gene_rd, scaffold_rd, gene_to_scaffold_maps):
  compn_ratio = calc_gene_scaf_rd_ratio(group[compn_col],
    gene_to_scaffold_maps[compn_col],
    indiv_gene_rd[compn_col],
    scaffold_rd[compn_col]
  )
  expn_ratio = calc_gene_scaf_rd_ratio(group[expn_col],
    gene_to_scaffold_maps[expn_col],
    indiv_gene_rd[expn_col],
    scaffold_rd[expn_col]
  )
  return (compn_ratio, expn_ratio)

# Calculate read depth ratios for ortho groups whose contributions from each
# genome are unequal.
def calc_rd_ratios(ortho_groups, threshold, read_depths, gene_to_scaffold_maps):
  #global_gene_rd = calc_1to1_avg_rd(ortho_groups, read_depths['gene'])
  #global_gene_rd = calc_global_avg_rd(read_depths['gene'])

  expn_rd_ratios = {
    'a': [],
    'b': [],
  }
  compn_rd_ratios = {
    'a': [],
    'b': [],
  }

  for group in ortho_groups:
    if len(group['a']) >= len(group['b']):
      expn_col, compn_col = 'a', 'b'
    else:
      expn_col, compn_col = 'b', 'a'

    expn_size = len(group[expn_col])
    compn_size = len(group[compn_col])

    size_ratio = expn_size / compn_size
    if size_ratio < threshold or compn_size != 1:
      # Not interesting. Proceed to next group.
      continue

    #compn_ratio, expn_ratio = calc_ratio_rel_to_global(
      #group, expn_col, compn_col, read_depths['gene'], global_gene_rd
    #)
    compn_ratio, expn_ratio = calc_ratio_rel_to_scaffold(
      group, expn_col, compn_col, read_depths['gene'], read_depths['scaffold'], gene_to_scaffold_maps
    )

    compn_rd_ratios[compn_col].append(compn_ratio)
    expn_rd_ratios[expn_col].append(expn_ratio)

  return {'expn': expn_rd_ratios, 'compn': compn_rd_ratios}

def main():
  parser = argparse.ArgumentParser(description='LOL HI2U')
  parser.add_argument('-t', '--group-threshold', dest='group_threshold', type=float, default=2,
    help='Group size ratio threshold above/below which group will be considered expanded/contracted')
  parser.add_argument('ortho_groups_fn')
  parser.add_argument('gene_overall_rd_fn_a')
  parser.add_argument('gene_overall_rd_fn_b')
  parser.add_argument('scaffold_overall_rd_fn_a')
  parser.add_argument('scaffold_overall_rd_fn_b')
  parser.add_argument('gene_to_scaffold_map_a')
  parser.add_argument('gene_to_scaffold_map_b')
  args = parser.parse_args()

  with open(args.ortho_groups_fn) as og:
    ortho_groups = json.load(og)['groups']

  rd_fns = {
    'gene': {
      'a': args.gene_overall_rd_fn_a,
      'b': args.gene_overall_rd_fn_b,
    },
    'scaffold': {
      'a': args.scaffold_overall_rd_fn_a,
      'b': args.scaffold_overall_rd_fn_b,
    }
  }

  read_depths = {}
  for result_type in ('gene', 'scaffold'):
    read_depths[result_type] = {}
    for genome in ('a', 'b'):
      with open(rd_fns[result_type][genome]) as f:
        read_depths[result_type][genome] = json.load(f)

  gene_to_scaffold_map_fns = {
    'a': args.gene_to_scaffold_map_a,
    'b': args.gene_to_scaffold_map_b,
  }

  gene_to_scaffold_maps = {}
  for genome, fn in gene_to_scaffold_map_fns.items():
    with open(fn) as f:
      gene_to_scaffold_maps[genome] = json.load(f)

  rd_ratios = calc_rd_ratios(ortho_groups, args.group_threshold, read_depths, gene_to_scaffold_maps)
  print(json.dumps(rd_ratios))

main()
