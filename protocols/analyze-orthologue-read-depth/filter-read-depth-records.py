#!/usr/bin/env python3
import json
import sys

def main():
  gene_read_depths_fn = sys.argv[1]
  interesting_genes_fn = sys.argv[2]

  with open(gene_read_depths_fn) as gene_read_depths_fd:
    read_depths = json.load(gene_read_depths_fd)
  with open(interesting_genes_fn) as interesting_genes_fd:
    gene_names = set([l.strip() for l in interesting_genes_fd.readlines()])

  retained_depths = {}
  for gene_name, depth in read_depths.items():
    if gene_name in gene_names:
      retained_depths[gene_name] = depth

  print(json.dumps(retained_depths))

main()
