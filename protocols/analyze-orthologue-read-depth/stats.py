#!/usr/bin/env python3
import numpy as np
import sys

def main():
  vals = [float(l.strip()) for l in sys.stdin]
  stats = (
    ('mean', np.mean(vals)),
    ('stdev', np.std(vals)),
    ('2.5 percentile', np.percentile(vals, 2.5)),
    ('10th percentile', np.percentile(vals, 10)),
    ('25th percentile', np.percentile(vals, 25)),
    ('median', np.median(vals)),
    ('75th percentile', np.percentile(vals, 75)),
    ('90th percentile', np.percentile(vals, 90)),
    ('97.5 percentile', np.percentile(vals, 97.5)),
    ('min', min(vals)),
    ('max', max(vals)),
  )
  max_label_len = max([len(label) for label, val in stats])
  for label, val in stats:
    print('%s%s' % ((label + ':').ljust(max_label_len + 2), val))

main()
