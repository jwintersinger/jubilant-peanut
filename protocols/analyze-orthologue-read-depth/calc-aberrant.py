#!/usr/bin/env python3
import argparse
import sys
import json
import numpy as np

def filter_gene_windows(gene_filter_list_fn, gene_windows):
  with open(gene_filter_list_fn) as f:
    gene_names = [l.strip() for l in f.readlines()]
    gene_names = set(gene_names)

  selected = {}
  for name, windows in gene_windows.items():
    if name in gene_names:
      selected[name] = windows

  return selected

def calculate_depth_threshold(gene_overall_rd, percentile):
  depths = list(gene_overall_rd.values())
  lower = np.percentile(depths, percentile)
  upper = np.percentile(depths, 100 - percentile)
  return (lower, upper)

def summarize_aberrant_windows(gene_windows, gene_overall_rd, percentile):
  lower, upper = calculate_depth_threshold(gene_overall_rd, percentile)
  aberrant_windows = {}
  for name, windows in gene_windows.items():
    filtered = [w for w in windows if w <= lower or w >= upper]
    if len(filtered) > 0:
      aberrant_windows[name] = filtered

  total_genes = len(gene_windows.keys())
  print('Genes considered:', total_genes)
  if total_genes == 0:
    return

  aberrant_genes = len(aberrant_windows.keys())
  print('Genes with aberrant windows: %s (%s%%)' % (
      aberrant_genes,
      100*(aberrant_genes / total_genes)
  ))
  if aberrant_genes == 0:
    return

  print('Average total windows per gene: ',
    sum([len(a) for a in gene_windows.values()]) / total_genes
  )
  print('Average aberrant windows per gene with aberrant windows: ',
    sum([len(a) for a in aberrant_windows.values()]) / aberrant_genes
  )

def main():
  parser = argparse.ArgumentParser(description='Determine how many aberrant windows lie in gene set')
  parser.add_argument('-d', '--depth-percentile', dest='depth_percentile', type=float, default=2.5,
    help='Percentile above/below which window will be considered aberrant')
  parser.add_argument('-f', '--filter-genes', dest='gene_filter_list_fn',
    help='Filename containing newline-separated list of gene subset to examine for aberrant windows')
  parser.add_argument('gene_windows_fn')
  parser.add_argument('gene_overall_rd_fn')
  args = parser.parse_args()

  with open(args.gene_windows_fn) as gw:
    gene_windows = json.load(gw)
  with open(args.gene_overall_rd_fn) as gor:
    gene_overall_rd = json.load(gor)

  # If list of gene names is supplised (one per line), only find aberrant
  # windows for those genes.
  if args.gene_filter_list_fn:
    gene_windows = filter_gene_windows(args.gene_filter_list_fn, gene_windows)
  summarize_aberrant_windows(gene_windows, gene_overall_rd, args.depth_percentile)

main()
