#!/usr/bin/env python3
import scipy.stats
import sys

def parse_vals(fn):
  with open(fn) as f:
    vals = []
    for line in f:
      vals.append(float(line.strip()))
  return vals

def main():
  fn1, fn2 = sys.argv[1], sys.argv[2]
  vals1, vals2 = parse_vals(fn1), parse_vals(fn2)
  print(scipy.stats.ks_2samp(vals1, vals2))

main()
