#!/usr/bin/env python3
import argparse
import json
import math
import sys

import matplotlib
# Force matplotlib not to use X11 backend, which produces exception when run
# over SSH.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

def remove_outliers(vals):
  # Throw away top 1% of most extreme data from both ends of distribution.
  vals = sorted(vals)
  n = int(len(vals) * 0.01)
  vals = vals[n:-n]
  return vals

def print_stats(vals):
  print('n:', len(vals))
  print('mean:', np.mean(vals)),
  print('stdev:', np.std(vals)),
  print('2.5 percentile:', np.percentile(vals, 2.5)),
  print('10th percentile:', np.percentile(vals, 10)),
  print('25th percentile:', np.percentile(vals, 25)),
  print('median:', np.median(vals)),
  print('75th percentile:', np.percentile(vals, 75)),
  print('90th percentile:', np.percentile(vals, 90)),
  print('97.5 percentile:', np.percentile(vals, 97.5)),
  print('min:', min(vals)),
  print('max:', max(vals)),

def plot(vals, graph_fn, xmin, xmax, ymin, ymax, num_bins):
  # Ensure that vals isn't empty so that plt.hist() and np.mean() calls don't
  # fail.
  if len(vals) == 0:
    vals = [-1]
  print_stats(vals)

  weights = np.ones_like(vals) / len(vals)
  bin_size = (xmax - xmin)/num_bins
  bins = np.arange(xmin, xmax, bin_size)

  plt.figure()
  plt.xticks(bins)
  counts, bins, patches = plt.hist(
    vals,
    bins=bins,
    weights=weights,
    log=False,
    facecolor='green',
    alpha=0.5,
  )

  bin_centers = 0.5 * np.diff(bins) + bins[:-1]
  for count, x in zip(counts, bin_centers):
    plt.annotate(
      '%.3f' % count,
      xy=(x, 1),
      xycoords=('data', 'axes fraction'),
      xytext=(0, -18),
      textcoords='offset points',
      va='top',
      ha='center',
      rotation=45
    )

  plt.xlim((xmin, xmax))
  plt.ylim((ymin, ymax))
  plt.savefig(graph_fn)

  print('Not on histogram:', 1 - sum(counts))

def plot_twoway(vals_a, vals_b, graph_filename, xmin, xmax, ymin, ymax, num_bins):
  # Ensure that vals isn't empty so that plt.hist() and np.mean() calls don't
  # fail.
  if len(vals_a) == 0:
    vals_a = [-1]
  if len(vals_b) == 0:
    vals_b = [-1]

  weights1 = np.ones_like(vals_a) / len(vals_a)
  weights2 = np.ones_like(vals_b) / len(vals_b)
  bin_size = (xmax - xmin)/num_bins
  bins = np.arange(xmin, xmax, bin_size)

  comphist(
    vals_a,
    vals_b,
    weights1=weights1,
    weights2=weights2,
    orientation='vertical',
    log=False,
    facecolor1='#491b3a',
    facecolor2='#1b4349',
    edgecolor='white',
    range=(xmin, xmax),
    bins=bins,
  )
  plt.xlim((xmin, xmax))
  plt.ylim((ymin, ymax))
  plt.savefig(graph_filename)

def comphist(x1, x2, orientation='vertical', **kwargs):
  """Draw a comparative histogram."""
  # Taken from http://log.ooz.ie/2013/02/matplotlib-comparative-histogram-recipe.html.

  # Split keyword args:
  kwargs1 = {}
  kwargs2 = {}
  kwcommon = {}
  for arg in kwargs:
    tgt_arg = arg[:-1]
    if arg.endswith('1'):
      arg_dict = kwargs1
    elif arg.endswith('2'):
      arg_dict = kwargs2
    else:
      arg_dict = kwcommon
      tgt_arg = arg
    arg_dict[tgt_arg] = kwargs[arg]
  kwargs1.update(kwcommon)
  kwargs2.update(kwcommon)

  matplotlib.rc('font', size=10)
  fig = plt.figure(figsize=(5, 4))

  # Have both histograms share one axis.
  if orientation == 'vertical':
    ax1 = plt.subplot(211)
    ax2 = plt.subplot(212, sharex=ax1)

    ax1.set_ylim([0, 0.5])
    ax2.set_ylim([0.5, 0])

    # Flip the ax2 histogarm horizontally.
    plt.setp(ax1.get_xticklabels(), visible=False)
    legend_loc = (1, 4)

  for ax in (ax1, ax2):
    ax.grid(True, linestyle='-', color='white', linewidth=0.5)
    for spine in ('top', 'bottom', 'right', 'left'):
      ax.spines[spine].set_color('white')#'#ababab')
    ax.tick_params(tick1On=False, tick2On=False)

  ax2.spines['top'].set_color('white')
  ax2.spines['bottom'].set_color('#9f9f9f')
  ax1.spines['left'].set_color('#9f9f9f')
  ax2.spines['left'].set_color('#9f9f9f')
  ax2.tick_params(axis='x', tick1On=True)

  ax1.hist(x1, orientation=orientation, **kwargs1)
  ax2.hist(x2, orientation=orientation, **kwargs2)
  ax1.legend(loc=legend_loc[0])
  ax2.legend(loc=legend_loc[1])
  # Tighten up the layout.
  plt.subplots_adjust(wspace=0.0, hspace=0.0)
  return fig

def parse_vals(fd):
  vals = []
  for line in fd:
    vals.append(float(line.strip()))
  return vals

def main():
  parser = argparse.ArgumentParser(description='Plot values on histogram')
  parser.add_argument('graph_fn', help='Graph filename')
  parser.add_argument('genome_rd_vals', help='Genome values', nargs='*')
  parser.add_argument('--xmin', dest='xmin', type=float, default=0)
  parser.add_argument('--xmax', dest='xmax', type=float, default=300)
  parser.add_argument('--ymin', dest='ymin', type=float, default=0)
  parser.add_argument('--ymax', dest='ymax', type=float, default=0.5)
  parser.add_argument('--bins', dest='num_bins', type=int, default=10)
  parser.add_argument('--remove-outliers', dest='remove_outliers', action='store_true')
  args = parser.parse_args()

  if len(args.genome_rd_vals) > 2:
    raise Exception('Too many genome RD args')

  if len(args.genome_rd_vals) == 2:
    with open(args.genome_rd_vals[0]) as f:
      vals_a = parse_vals(f)
    with open(args.genome_rd_vals[1]) as f:
      vals_b = parse_vals(f)
    if args.remove_outliers:
      vals_a = remove_outliers(vals_a)
      vals_b = remove_outliers(vals_b)
    plot_twoway(vals_a, vals_b, args.graph_fn, args.xmin, args.xmax, args.ymin, args.ymax, args.num_bins)

  else:
    if len(args.genome_rd_vals) == 1:
      with open(args.genome_rd_vals[0]) as f:
        vals = parse_vals(f)
    else:
      vals = parse_vals(sys.stdin)
    if args.remove_outliers:
      vals = remove_outliers(vals)
    plot(vals, args.graph_fn, args.xmin, args.xmax, args.ymin, args.ymax, args.num_bins)

if __name__ == '__main__':
  main()
