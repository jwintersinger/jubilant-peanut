#!/usr/bin/env python3
import argparse
import json
import sys

def find_interesting_groups(ortho_json_fd, genome_col, ortho_group_type, group_threshold):
  ortho_results = json.load(ortho_json_fd)
  ortho_groups = ortho_results['groups']
  other_col = (genome_col == 'a') and 'b' or 'a'
  interesting = []

  for group in ortho_groups:
    size_ratio = len(group[genome_col]) / len(group[other_col])
    if ortho_group_type == 'expanded' and size_ratio >= group_threshold and len(group[other_col]) == 1:
      interesting.append(group)
    elif ortho_group_type == 'contracted' and size_ratio <= 1/group_threshold and len(group[genome_col]) == 1:
      interesting.append(group)
    elif ortho_group_type == '1to1' and len(group[genome_col]) == len(group[other_col]) == 1:
      interesting.append(group)

  return interesting

def extract_gene_names(ortho_groups, genome_col):
  gene_names = []
  for group in ortho_groups:
    genes = group[genome_col]
    names = [g[0] for g in genes]
    gene_names += names
  return gene_names

def main():
  orthos_json_fn = sys.argv[1]
  genome_col = sys.argv[2]
  ortho_group_type = sys.argv[3]

  parser = argparse.ArgumentParser(description='Print gene names of genes above/below size ratio threshold')
  parser.add_argument('-t', '--group-threshold', dest='group_threshold', type=float, default=2,
    help='Group size ratio threshold above/below which group will be considered expanded/contracted')
  parser.add_argument('orthos_json_fn')
  parser.add_argument('genome_col')
  parser.add_argument('ortho_group_type', choices=('expanded', 'contracted', '1to1'))
  args = parser.parse_args()

  with open(args.orthos_json_fn) as orthos_json_fd:
    interesting_groups = find_interesting_groups(
      orthos_json_fd,
      args.genome_col,
      args.ortho_group_type,
      args.group_threshold,
    )
  interesting_genes = extract_gene_names(interesting_groups, args.genome_col)

  for gene in interesting_genes:
    print(gene)

main()
