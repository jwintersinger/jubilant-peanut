#!/bin/bash
set -euo pipefail

ROOT_DIR=~/work/jubilant-peanut
PROT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OUT_DIR=$ROOT_DIR/runs/analyze-read-depth

function align_reads {
  # None of the below code has been run from this script in the past -- all
  # commands were run manually. I leave it here for reference. Note it only
  # handles PRJEB506 and not PRJNA205202 presently.
  for foo in ERR*; do
    cd $foo
    bwa mem -t 8 ~/work/jubilant-peanut/data/PRJEB506/h_contortus.PRJEB506.WS239.genomic.fa ERR*.fastq.gz > alignment.sam 2> alignment.results &
    cd ..
  done
  wait

  for foo in ERR*; do
    cd $foo
    samtools view -bS alignment.sam > alignment.bam
    # Allow 6 GiB memory usage.
    samtools sort -m 6442450944 alignment.bam alignment.sorted
    cd ..
  done &
  wait

  samtools merge combined.bam ERR*/alignment.sorted.bam
}

function calc_read_depth {
  cd $OUT_DIR/mhco3
  $PROT_DIR/calc-read-depth.py \
    $ROOT_DIR/data/reads/PRJEB506/illumina/combined.bam \
    $ROOT_DIR/data/PRJEB506/h_contortus.PRJEB506.WS239.annotations.gff3 &

  cd $OUT_DIR/mcmaster
  $PROT_DIR/calc-read-depth.py \
    $ROOT_DIR/data/reads/PRJNA205202/combined.bam \
    $ROOT_DIR/data/PRJNA205202/h_contortus.PRJNA205202.WS239.annotations.gff3 &

  wait
}

function print_header {
  text=$1
  chr=$2

  count=$(echo -n $text | wc -c)
  printf -v str "%-${count}s" ' '
  sep="${str// /${chr}}"
  echo -e "${sep}\n${text}\n${sep}"
}

function process_read_depth {
  ortho_results=$ROOT_DIR/runs/orthomcl/PRJEB506-PRJNA205202/results/orthomcl_results.json
  group_threshold=2
  depth_percentile=1

  $PROT_DIR/map-genes-to-scaffolds.py \
    $ROOT_DIR/data/PRJEB506/genes.gff3 \
    > $OUT_DIR/mhco3/gene_to_scaffold_map.json
  $PROT_DIR/map-genes-to-scaffolds.py \
    $ROOT_DIR/data/PRJNA205202/genes.gff3 \
    > $OUT_DIR/mcmaster/gene_to_scaffold_map.json

  for genome in {mhco3,mcmaster}; do
    cd $OUT_DIR/$genome
    print_header $genome =

    if [[ $genome == 'mhco3' ]]; then
      ortho_col=a
    else
      ortho_col=b
    fi

    for foo in {gene,scaffold}_overall_read_depth.json; do
      print_header $foo -
      jq '.[]' $foo > $foo.values
      cat $foo.values | \
        $PROT_DIR/plot.py \
        --remove-outliers \
        $foo.png
      echo
    done

    print_header 'Aberrant events' -
    $PROT_DIR/calc-aberrant.py \
      --depth-percentile $depth_percentile \
      gene_windows_read_depths.json \
      gene_overall_read_depth.json
    echo

    for ortho_group_type in expanded contracted 1to1; do
      print_header $ortho_group_type -
      interesting_genes=$OUT_DIR/$genome/$ortho_group_type.genes

      echo 'List of' $ortho_group_type 'genes:' $interesting_genes
      # Technically, only need pass whichever of
      # --{expanded,contracted}-threshold is needed based on whether doing
      # expanded/contracted run, but passing both makes shell script simpler.
      $PROT_DIR/list-interesting-genes.py \
        --group-threshold $group_threshold \
        $ortho_results \
        $ortho_col \
        $ortho_group_type \
        > $interesting_genes

      $PROT_DIR/filter-read-depth-records.py \
        gene_overall_read_depth.json \
        $interesting_genes \
        | jq '.[]' \
        > ${ortho_group_type}.values

      cat ${ortho_group_type}.values \
        | $PROT_DIR/plot.py \
        --remove-outliers \
        ${ortho_group_type}_groups.png

      echo
      $PROT_DIR/calc-aberrant.py \
        --depth-percentile $depth_percentile \
        --filter-genes $interesting_genes \
        gene_windows_read_depths.json \
        gene_overall_read_depth.json
      echo
    done
  done

  cd $OUT_DIR
  print_header Comparison =
  $PROT_DIR/compare-nto1-rd.py \
    $ortho_results \
    {mhco3,mcmaster}/gene_overall_read_depth.json \
    {mhco3,mcmaster}/scaffold_overall_read_depth.json \
    {mhco3,mcmaster}/gene_to_scaffold_map.json \
    > rd_ratios.json

  for event_type in expn compn; do
    for genome in a b; do
      echo $event_type.$genome
      cat rd_ratios.json | \
        jq ".${event_type}.${genome}|.[]" | \
        $PROT_DIR/plot.py \
        --xmin 0 \
        --xmax 2.5 \
        $event_type.$genome.png
      echo
    done

    continue
    # Plot McMaster above MHco3.
    $PROT_DIR/plot.py \
      --xmin 0 \
      --xmax 2.5 \
      $event_type.png \
      <(cat rd_ratios.json | \
        jq ".${event_type}.b|.[]") \
      <(cat rd_ratios.json | \
        jq ".${event_type}.a|.[]")
  done
}

function main {
  mkdir -p $OUT_DIR/{mcmaster,mhco3}

  #align_reads
  calc_read_depth
  process_read_depth
}

main
