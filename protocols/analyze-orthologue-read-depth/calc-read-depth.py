#!/usr/bin/env python2

from __future__ import print_function
import HTSeq
import sys
import numpy as np
import pickle
import json
from multiprocessing import Process
from datetime import datetime

def log(msg):
  print('[%s] %s' % (datetime.now(), msg))

class Util(object):
  @staticmethod
  def each_strand(chrom_vectors):
    for chrom in chrom_vectors.keys():
      for strand in chrom_vectors[chrom].keys():
        assert strand in ('+', '-', '.'), ('Unknown strand: "%s"' % strand)
        yield chrom, strand, chrom_vectors[chrom][strand]

  @staticmethod
  def find_chromosome_length(chrom_vec):
    '''
    Suppose a chromosome spans [0, end). This method returns end.
    '''
    # Get last step to determine max length .
    for step in chrom_vec.steps():
      pass
    last_step_interval, last_step_read_depth = step

    # Ensure last step proceeds to end of "indefinitely long" chromosome (in
    # this case, indefinite is represented by HTSeq as sys.maxint, which, I
    # should note, has been removed in Python 3).
    assert last_step_interval.end == sys.maxint
    # Ensure read depth for "indefinitely long" chromsome is zero, since no
    # reads should be associated with this interval.
    assert last_step_read_depth == 0

    # Start coordinate of last interval should represent (exclusive) end
    # coordinate of last read on chromosome.
    chromosome_end = last_step_interval.start
    return chromosome_end

class CoverageFinder(object):
  def __init__(self, alignment_filename):
    self._alignment_filename = alignment_filename

  def determine_coverage(self):
    alignment_file = HTSeq.BAM_Reader(self._alignment_filename)
    # stranded=False, as whatever strand a given read has been assigned to in our
    # alignment file is irrelevant. We wish only to find enriched windows without
    # considering strandedness. Note that though individual GenomicArrays
    # specifying regions in cvg will be stranded, their strandedness will be
    # ignored by cvg.
    cvg = HTSeq.GenomicArray('auto', stranded=False, typecode='i')
    aligned_base_count = 0

    for aln in alignment_file:
      if aln.aligned:
        interval = aln.iv

        cvg[aln.iv] += 1
        # This value counts reads aligned to both strands, so it will be
        # approximately twice that of genome.
        aligned_base_count += aln.iv.length

    return (cvg, aligned_base_count)

def extract_gene_name(name):
  prefix = 'gene:'
  if not name.startswith(prefix):
    raise Exception('Not a gene: %s' % name)
  name = name[len(prefix):]
  return name

def calc_overall_windows_read_depth(cvg):
  enriched = []
  window_size = 100

  for chr_name, strand, chrom_vec in Util.each_strand(cvg.chrom_vectors):
    chromosome_length = Util.find_chromosome_length(chrom_vec)
    windows = []
    for coord_start in range(0, chromosome_length, window_size):
      coord_end = coord_start + window_size
      window = HTSeq.GenomicInterval(chr_name, coord_start, coord_end, strand)
      window_vec = cvg[window]
      window_cvg = list(window_vec.values()) # Convert from generator to list for numpy.
      mean = np.mean(window_cvg)
      windows.append((chr_name, strand, coord_start, coord_end, mean))

  with open('overall_windows_read_depth.json', 'w') as f:
    json.dump(windows, f)

def calc_scaffold_overall_read_depth(cvg):
  log('Starting calc_scaffold_overall_read_depth')
  mean_cvg = {}
  for chr_name, strand, chrom_vec in Util.each_strand(cvg.chrom_vectors):
    chr_len = Util.find_chromosome_length(chrom_vec)
    window = HTSeq.GenomicInterval(chr_name, 0, chr_len, strand)
    vec = cvg[window]
    values = list(vec.values())
    mean = np.mean(values)
    mean_cvg['%s_%s' % (chr_name, strand)] = mean
  with open('scaffold_overall_read_depth.json', 'w') as f:
    json.dump(mean_cvg, f)

def calc_gene_overall_read_depth(cvg, gff_fn):
  log('Starting calc_gene_overall_read_depth')
  gff = HTSeq.GFF_Reader(gff_fn)
  means = {}

  for feature in gff:
    if feature.type.lower() != 'gene':
      continue
    name = extract_gene_name(feature.name)

    window_vec = cvg[feature.iv]
    read_counts = list(window_vec.values())
    mean = np.mean(read_counts)
    means[name] = mean

  with open('gene_overall_read_depth.json', 'w') as f:
    json.dump(means, f)

def calc_gene_windows_read_depths(cvg, gff_fn):
  log('Starting calc_gene_windows_read_depths')
  depths = {}
  window_size = 100
  gff = HTSeq.GFF_Reader(gff_fn)

  for feature in gff:
    if feature.type.lower() != 'gene':
      continue
    name = extract_gene_name(feature.name)
    interval = feature.iv

    window_means = []
    for coord_start in range(interval.start, interval.end, window_size):
      coord_end = coord_start + window_size
      window = HTSeq.GenomicInterval(feature.iv.chrom, coord_start, coord_end, feature.iv.strand)
      window_vec = cvg[window]
      window_cvg = list(window_vec.values())
      mean = np.mean(window_cvg)
      window_means.append(mean)
    depths[name] = window_means

  with open('gene_windows_read_depths.json', 'w') as f:
    json.dump(depths, f)

def run_serial(funcs):
  for func, args in funcs:
    func(*args)

def run_parallel(funcs):
  processes = []
  for func, args in funcs:
    processes.append(Process(target=func, args=args))
  [p.start() for p in processes]
  [p.join() for p in processes]

def main():
  bam_file = sys.argv[1]
  gff_fn = sys.argv[2]

  log('Begin calculating cvg')
  cvg, aligned_base_count = CoverageFinder(bam_file).determine_coverage()
  log('Done calculating cvg')
  log('Aligned bases: %s' % aligned_base_count)

  funcs = (
    #(calc_scaffold_overall_read_depth, (cvg,)),
    #(calc_gene_overall_read_depth,     (cvg, gff_fn)),
    #(calc_gene_windows_read_depths,    (cvg, gff_fn)),
    (calc_overall_windows_read_depth,  (cvg,)),
  )
  # Running in parallel may cause system to run out of memory, as these are
  # memory-intensive analyses.
  #run_parallel(funcs)
  run_serial(funcs)

if __name__ == '__main__':
  main()
