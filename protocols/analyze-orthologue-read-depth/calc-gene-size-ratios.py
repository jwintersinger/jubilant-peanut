#!/usr/bin/env python2
from __future__ import division
import HTSeq
import sys
from dregs import binf

def calc_scaffold_lengths(fasta_fn):
  lengths = {}
  with open(fasta_fn) as fasta_fd:
    for seq_id, seq in binf.parse_fasta(fasta_fd):
      scaffold = seq_id.split()[0]
      lengths[scaffold] = len(seq)
  return lengths

def main():
  gff_fn = sys.argv[1]
  fasta_fn = sys.argv[2]
  if len(sys.argv) == 4:
    gene_names_fn = sys.argv[3]
  else:
    gene_names_fn = None

  if gene_names_fn is not None:
    with open(gene_names_fn) as f:
      gene_names = set([l.strip() for l in f])
  else:
    gene_names = None

  gff = HTSeq.GFF_Reader(gff_fn)
  scaffold_lengths = calc_scaffold_lengths(fasta_fn)

  for feature in gff:
    if feature.type.lower() != 'gene':
      continue

    gene_name = feature.attr['Name']
    if gene_names is not None and gene_name not in gene_names:
      continue

    gene_scaffold = feature.iv.chrom
    length_ratio = feature.iv.length / scaffold_lengths[gene_scaffold]
    print(length_ratio)

main()
