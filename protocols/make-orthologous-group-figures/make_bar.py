#!/usr/bin/env python3

import matplotlib
# Force matplotlib not to use X11 backend, which produces exception when run
# over SSH.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

def plot_multi_ortho_scaffolds():
  matplotlib.rcParams['axes.titlesize'] = 15
  matplotlib.rcParams['axes.labelsize'] = 15
  matplotlib.rcParams['xtick.labelsize'] = matplotlib.rcParams['ytick.labelsize'] = 15

  fig, ax = plt.subplots()
  plt.xlabel('Genome')
  plt.ylabel('Number of scaffolds')
  plt.title('Scaffolds with multiple members of single\northologous group in each genome')
  width = 0.8
  ind = np.arange(2)
  plt.xlim(-0.3, 2.1)
  #ax.patch.set_facecolor('none')
  ax.bar(
    ind,
    [564, 118],
    width,
    color=('#1b4349', '#491b3a'),
  )

  ax.set_xticks(ind + 0.4)
  ax.set_xticklabels(['MHco3', 'McMaster'])
  #sns.despine()
  plt.savefig('ortho_multi.svg', bbox_inches='tight')

def plot_orthology():
  colors = {
    'MH': '#8dd3c7',
    'Mc': '#ffffb3',
    'Ce': '#bebada',
    'Cb': '#fb8072'
  }
  colors = {
    'MH': '#1b9e77',
    'Mc': '#d95f02',
    'Ce': '#7570b2',
    'Cb': '#e7298a'
  }

  orthology = [
    (
      ('MH', 'Mc', 14539/21897),
      ('Mc', 'MH', 10562/23610),
    ),
    (
      ('Ce', 'Cb', 15095/20511),
      ('Cb', 'Ce', 14550/21842),
    ),
    (
      ('MH', 'Ce', 12977/21897, 12977/21897),
      ('Ce', 'MH', 9283/20511, 9283/20511),
    ),
    (
      ('Mc', 'Ce', 8393/23610),
      ('Ce', 'Mc', 8699/20511)
    )
  ]

  left_bars = [g[0] for g in orthology]
  right_bars = [g[1] for g in orthology]
  
  ind = np.arange(len(orthology))
  width = 0.4
  sns.set_style('darkgrid', {'axes.axisbelow': True, 'axes.edgecolor': 3*(0.4,)})

  #{'axes.edgecolor': edge_color, 'grid.color': edge_color, 'xtick.color': edge_color, 'ytick.color': edge_color, 'axes.axisbelow': False}
  fig, ax = plt.subplots()
  ax.bar(
    ind,
    [b[2] for b in left_bars],
    width,
    color = [colors[b[0]] for b in left_bars],
    linewidth=0,
  )
  ax.bar(
    ind + width,
    [b[2] for b in right_bars],
    width,
    color = [colors[b[0]] for b in right_bars],
    linewidth=0,
  )

  label_coords = ind + width/2 + 0.1
  label_coords = sorted(list(label_coords) + list(label_coords + width))
  flattened = sum(orthology, tuple())
  labels = ['%s-%s' % g[:2] for g in flattened]

  ax.set_xticks(label_coords)
  ax.set_xticklabels(labels)
  formatter = matplotlib.ticker.FuncFormatter(lambda y, position: str(int(100*y)) + '%')
  plt.gca().yaxis.set_major_formatter(formatter)

  locs, labels = plt.xticks()
  plt.setp(labels, rotation=-45)
  plt.xlim(-width, len(orthology) + (width - (1 - 2*width)))

  sns.despine()
  plt.grid(False, axis='x')
  #plt.grid(True, axis='y', color='white', linestyle='-', linewidth=0.8)
  plt.savefig('orthology_bar.svg', bbox_inches='tight')

def main():
  plot_multi_ortho_scaffolds()
  plot_orthology()

main()
