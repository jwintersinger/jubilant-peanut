#!/bin/bash
set -euo pipefail
PROTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd ../../runs
mkdir -p ortho-group-figures && cd ortho-group-figures

$PROTDIR/make_pie.R
$PROTDIR/make_bar.py
