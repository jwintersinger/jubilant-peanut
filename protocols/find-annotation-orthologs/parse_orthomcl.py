#!/usr/bin/env python3
'''
Convert OrthoMCL groups.txt to JSON-formatted file. Output format should be the
same as that produced by parse_inparanoid.py.

Usage: cat <OrthoMCL groups.txt file> | parse_orthomcl.py > parsed.json
'''

import sys
import json

def parse(orthomcl_fd, orthomcl_name_a, orthomcl_name_b):
  groups = []

  for line in orthomcl_fd:
    group_id, members = line.split(': ', 1)
    proteins = members.split()
    group = {
      'a': [],
      'b': [],
    }

    for protein in proteins:
      protein_group, protein_name = protein.split('|', 1)
      if protein_group == orthomcl_name_a:
        group_name = 'a'
      elif protein_group == orthomcl_name_b:
        group_name = 'b'

      # Hack: since we don't have the score readily at hand, just set to zero
      # for compatibility with our InParanoid JSON format.
      score = 0
      group[group_name].append((
        protein_name,
        score,
      ))

    # Ignore groups that don't have at least one protein from each genome.
    if len(group['a']) == 0 or len(group['b']) == 0:
      continue

    groups.append(group)
  return groups


def main():
  orthomcl_name_a = sys.argv[1]
  orthomcl_name_b = sys.argv[2]
  results = {
    'groups': parse(sys.stdin, orthomcl_name_a, orthomcl_name_b)
  }
  print(json.dumps(results))

main()
