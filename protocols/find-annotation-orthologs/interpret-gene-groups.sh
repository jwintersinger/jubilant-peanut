#!/bin/bash
set -euo pipefail

gg=$1

# How many scaffolds have >1 orthologue on them?
for genome in a b; do
  echo -n 'Multiple orthologues on one scaffold in genome' "$genome: "
  cat $gg | grep ^${genome} | awk '{ if ($4 > 1) s+=1 } END {print s}'
  cat $gg | grep ^${genome} | tr -s ' ' | cut -d' ' -f 4 | grep -v '^1$' | sort | uniq -c | sort -n
done
echo

for genome in a b; do
  echo -n 'Cases where no non-ortho genes on scaffold in genome' "$genome: "
  cat $gg | grep ^${genome} | awk '{ if ($4 > 1 && $4 == $5) print $0 }' | wc -l
done
echo

for genome in a b; do
  echo -n 'Cases where non-ortho genes on scaffold (not overlapping the group) in genome' "$genome: "
  cat $gg | grep ^${genome} | awk '{ if ($4 > 1 && $6 == "True" && $5 > $4) print $0 }' | wc -l
done
echo

# How many scaffolds have more than one orthologue on them ($4 > 1), and have
# their ortho group not tandemly arrayed ($6 == False)?
for genome in a b; do
  echo -n 'Cases where non-ortho genes on scaffold (overlapping the group) in genome' "$genome: "
  # $5 > $4 is redundant -- if not contiguous, we *must* have non-ortho
  # genes on scaffold.
  cat $gg | grep ^${genome} | awk '{ if ($4 > 1 && $6 == "False" && $5 > $4) print $0 }' | wc -l
done
