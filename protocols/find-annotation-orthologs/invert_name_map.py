#!/usr/bin/env python3
# Given JSON-formatted InParanoid results, invert the name mapping we did prior
# to running InParanoid, restoring the original names.

import json
import sys

def load_json(filename):
  with open(filename) as f:
    return json.load(f)

def main():
  results_filename = sys.argv[1]
  map_a_filename = sys.argv[2]
  map_b_filename = sys.argv[3]
  results = load_json(results_filename)
  name_maps = {
    'a': load_json(map_a_filename),
    'b': load_json(map_b_filename),
  }

  for group in results['groups']:
    for key in ('a', 'b'):
      for index, vals in enumerate(group[key]):
        name = vals[0]
        invmap_name = name_maps[key][name]
        vals[0] = invmap_name
        group[key][index] = vals

  print(json.dumps(results))

if __name__ == '__main__':
  main()
