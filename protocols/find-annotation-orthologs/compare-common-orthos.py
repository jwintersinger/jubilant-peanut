#!/usr/bin/env python3
from collections import Counter
import json
import sys

def load_set(fn, col):
  with open(fn) as f:
    results = json.load(f)
  groups = results['groups']

  genes = []
  for group in groups:
    for gene_name, score in group[col]:
      genes.append(gene_name)

  return set(genes)

def calc_counts(set_a, set_b):
  union = set_a.union(set_b)
  indicators = [2*'%d' % (e in set_a, e in set_b) for e in union]
  counts = Counter(indicators)

  counts_incl_empty = {}
  for a in (0, 1):
    for b in (0, 1):
      key = 2*'%s' % (a, b)
      counts_incl_empty[key] = counts[key]

  return counts_incl_empty

def plot_venn(labels, counts):
  labels_str = ', '.join(["'%s'" % s for s in labels])
  weights_str = ', '.join(["'%s' = %s" % (k, v) for k, v in counts.items()])
  print('''
    library(Vennerable)

    args <- commandArgs(trailingOnly = TRUE)
    svg_fn <- args[1]
    v <- Venn(SetNames = c(%s), Weight = c(%s))
    svg(svg_fn)
    plot(v, doWeights = TRUE, type='circles', show = list(SetLabels=TRUE))
    dev.off()
  ''' % (labels_str, weights_str))

def print_summation(set_a, set_b, set_a_fn, set_b_fn):
  print('Common: %s' % len(set_a & set_b))
  print('Unique to %s: %s' % (set_a_fn, len(set_a - set_b)))
  print('Unique to %s: %s' % (set_b_fn, len(set_b - set_a)))

def main():
  set_a_fn = sys.argv[1]
  col_a = sys.argv[2]
  set_b_fn = sys.argv[3]
  col_b = sys.argv[4]

  set_a = load_set(set_a_fn, col_a)
  set_b = load_set(set_b_fn, col_b)
  counts = calc_counts(set_a, set_b)

  labels = ('Orthos from\\nC. elegans to MHco3', 'Orthos from\\nC. elegans to McMaster')
  plot_venn(labels, counts)
  #print_summation(set_a, set_b, set_a_fn, set_b_fn)

main()
