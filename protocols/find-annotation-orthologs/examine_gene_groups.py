#!/usr/bin/env python2

import json
import re
import sqlite3
import sys

class GeneManager(object):
  def __init__(self):
    self._conn = sqlite3.connect(':memory:')
    # Give dictionary-based access to results.
    self._conn.row_factory = sqlite3.Row
    self._create_db()

  def _create_db(self):
    cursor = self._cursor()
    # Note that group_name refers to name of genome.
    cursor.execute('''CREATE TABLE genes (
      id         INTEGER PRIMARY KEY,
      group_name TEXT,
      name       TEXT,
      seqid      TEXT,
      tstart     INTEGER,
      tend       INTEGER,
      strand     TEXT
    )''')
    cursor.execute('CREATE INDEX idx_name ON genes(name)')
    cursor.execute('CREATE INDEX idx_seqid_strand ON genes(seqid, strand)')
    self._commit()

  def _insert_genes(self, genes, group_name):
    cursor = self._cursor()

    for gid, gene in genes.items():
      cursor.execute(
        'INSERT INTO genes (group_name, name, seqid, tstart, tend, strand) VALUES (?, ?, ?, ?, ?, ?)', (
        group_name,
        gid,
        gene['seqid'],
        gene['tstart'],
        gene['tend'],
        gene['strand'],
      ))

    self._commit()

  def _parse_gff(self, gff_filename):
    genes = {}

    with open(gff_filename) as gff_file:
      for line in gff_file:
        line = line.strip()
        if line == '' or line.startswith('#'):
          continue

        fields = line.split('\t')
        if not fields[2].lower() == 'gene':
          continue

        attribs = dict([f.split('=', 1) for f in fields[8].split(';')])
        if 'ID' not in attribs:
          continue
        id_tokens = attribs['ID'].split(':', 1)
        if not id_tokens[0].lower() == 'gene':
          continue
        gid = id_tokens[1]

        if gid in genes:
          raise Exception('Duplicate seqid %s' % seqid)

        genes[gid] = {
          'seqid':  fields[0],
          'tstart':  int(fields[3]),
          'tend':    int(fields[4]),
          'strand': fields[6],
        }

    return genes

  def _cursor(self):
    return self._conn.cursor()

  def _commit(self):
    self._conn.commit()

  def parse(self, gff_filename, group_name):
    genes = self._parse_gff(gff_filename)
    self._insert_genes(genes, group_name)

  def close(self):
    self._conn.close()

  def _determine_intervening_genes(self, row, placeholders, gene_names):
    cursor = self._cursor()

    # This query finds all genes on the given ortho group's scaffold that
    # aren't part of the ortho group. It then counts how many of these overlap
    # the group by determining the number for which this condition is true:
    # MIN(t1.tend, group_end) > MAX(t1.tstart, group_start).
    #
    # Note that intervening_genes may be >0 even when number_of_genes_in_group
    # = 1. This occurs if other genes overlap the single gene in
    # the group.
    cursor.execute('''
      SELECT COUNT(*) AS intervening_genes
      FROM genes t1
      WHERE
        t1.name NOT IN (%s)  AND
        t1.group_name = ?    AND
        t1.seqid = ?         AND
        t1.strand = ?        AND
        (
          (CASE WHEN t1.tend   < ? THEN t1.tend   ELSE ? END) -- MIN(t1.tend, placeholder)
          >
          (CASE WHEN t1.tstart > ? THEN t1.tstart ELSE ? END) -- MAX(t1.tstart, placeholder)
        )
      ''' % placeholders, gene_names + [
        row['group_name'],
        row['seqid'],
        row['strand'],
        row['group_end'],
        row['group_end'],
        row['group_start'],
        row['group_start'],
      ]
    )

    result = cursor.fetchone()
    row['intervening_genes'] = result['intervening_genes']

  def find_scaffolds(self, gene_names):
    cursor = self._cursor()
    placeholders     = ', '.join(['?' for t in gene_names])
    cursor.execute('''
      SELECT
        t1.group_name,
        t1.seqid,
        t1.strand,
        COUNT(t1.seqid) AS orthologues_on_scaffold_count,
        (
          SELECT COUNT(*)
          FROM genes t2
          WHERE
            t2.seqid = t1.seqid AND
            t2.strand = t1.strand
        ) AS genes_on_scaffold_count,
        MIN(t1.tstart) AS group_start,
        MAX(t1.tend)   AS group_end
      FROM genes t1
      WHERE t1.name IN (%s)
      GROUP BY t1.seqid, t1.strand, t1.group_name
      ORDER BY t1.group_name, t1.seqid
      ''' % placeholders,
      gene_names
    )
    rows = cursor.fetchall()
    rows = [dict(r) for r in rows]
    for row in rows:
      self._determine_intervening_genes(row, placeholders, gene_names)

    # As any genes not in DB for "WHERE t1.name IN (...)" will silently
    # be omitted from SQL result set, we must ensure that the sizes of the
    # query and results sets match.
    genes_on_scaffolds_sum = sum([t['orthologues_on_scaffold_count'] for t in rows])
    if genes_on_scaffolds_sum != len(gene_names):
      from pprint import pprint
      pprint(repr(gene_names))
    assert genes_on_scaffolds_sum == len(gene_names), \
      'Some genes lack associated scaffolds (%s, %s)' % (genes_on_scaffolds_sum, len(gene_names))

    return rows


# Natural sorting technique taken from http://stackoverflow.com/a/2669120/1691611.
#def sort_scaffold_counts(scaffold_counts):
  #convert      = lambda text: int(text) if text.isdigit() else text
  #alphanum_key = lambda key:  [convert(c) for c in re.split('([0-9]+)', key)]
  #scaffold_counts.sort(key=lambda c: alphanum_key(c[1]))

def process_ortho_group(ogroup, gene_manager):
  for gname in ogroup.keys():
    genes = []
    for seqname, score in ogroup[gname]:
      name = seqname.split()[0]
      genes.append(name)

    for row in gene_manager.find_scaffolds(genes):
      print('%s %-20s %-4s %2s %2s %-5s %s' % (
        row['group_name'],
        row['seqid'],
        row['strand'],
        row['orthologues_on_scaffold_count'],
        row['genes_on_scaffold_count'],
        # Boolean indicating whether orthologues tandemly arrayed.
        row['intervening_genes'] == 0,
        row['intervening_genes'],
      ))

def process_ortho_groups(ortho_groups, gene_manager):
  already_printed_first = False

  for ogroup in ortho_groups:
    a_len, b_len = len(ogroup['a']), len(ogroup['b'])
    # We aren't interested in 1:1 relationships between orthologues, as we
    # already know each orthologue lies on a single scaffold.
    if a_len == b_len == 1:
      continue

    if already_printed_first:
      print('')
    else:
      already_printed_first = True

    print('Group (a:b = %s:%s)' % (a_len, b_len))
    process_ortho_group(ogroup, gene_manager)

def examine_contiguity(ortho_groups, gene_fnames):
  tm = GeneManager()
  for gname in ('a', 'b'):
    tm.parse(gene_fnames[gname], gname)
  process_ortho_groups(ortho_groups, tm)

def main():
  gene_fnames = {
    'a': sys.argv[1],
    'b': sys.argv[2],
  }
  ortho_groups = json.load(sys.stdin)['groups']
  examine_contiguity(ortho_groups, gene_fnames)

if __name__ == '__main__':
  #import cProfile
  #cProfile.run('main()')
  main()
