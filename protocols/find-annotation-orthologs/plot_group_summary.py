#!/usr/bin/env python3

import json
import math
import sys

import matplotlib
# Force matplotlib not to use X11 backend, which produces exception when run
# over SSH.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns

def plot_groups(groups, filename, make_y_scale_log, group_a_label, group_b_label):
  vals = []

  for group in groups:
    a_len = len(group['a'])
    b_len = len(group['b'])
    log = math.log(float(a_len) / float(b_len), 2)
    vals.append(log)

  fig, ax = plt.subplots(figsize=(5, 4))

  bins = list(range(int(math.floor(min(vals))), int(math.ceil(max(vals))) + 1))
  params = {'text.usetex': False, 'mathtext.fontset': 'stixsans'}
  plt.rcParams.update(params)
  plt.figure()
  plt.xticks(bins)
  plt.hist(vals, bins=bins, log=make_y_scale_log, facecolor='#48a748', edgecolor='white')

  plt.title('Distribution of orthologous group gene counts')
  plt.xlabel(r'$log_2\ \frac{%s}{%s}$' % (group_a_label, group_b_label))
  plt.ylabel('Occurrences')
  plt.yscale('symlog')
  plt.ylim([0.1, 10**4])

  edge_color = 3*(0.7,)
  sns.set_style('ticks', {'axes.edgecolor': edge_color, 'grid.color': edge_color, 'xtick.color': edge_color, 'ytick.color': edge_color, 'axes.axisbelow': False})
  [i.set_color("black") for i in plt.gca().get_xticklabels() + plt.gca().get_yticklabels()]
  sns.despine()
  plt.grid(True, axis='y', color='white', linestyle='-', linewidth=0.5)

  plt.savefig(filename, bbox_inches='tight')

def main():
  group_a_label = sys.argv[1]
  group_b_label = sys.argv[2]
  output_linear = sys.argv[3]
  output_log    = sys.argv[4]

  matplotlib.rc('font', size=14)

  ortho_groups = json.load(sys.stdin)['groups']
  plot_groups(ortho_groups, output_linear, False, group_a_label, group_b_label)
  plot_groups(ortho_groups, output_log,    True,  group_a_label, group_b_label)

if __name__ == '__main__':
  main()
