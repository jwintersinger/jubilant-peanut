#!/bin/bash

function run_inparanoid {
  OUTDIR=../../runs/inparanoid
  mkdir -p $OUTDIR
  EXTRA_ARGS="--only-analyze"

  for matrix in BLOSUM{62,80}; do
    # Haemonchus against Haemonchus.
    ./run_inparanoid.py \
      --matrix $matrix \
      --outgroup PRJNA13758 \
      $EXTRA_ARGS \
      PRJEB506 PRJNA205202 \
      $OUTDIR &
    ./run_inparanoid.py \
      --matrix $matrix \
      $EXTRA_ARGS \
      PRJEB506 PRJNA205202 \
      $OUTDIR &

    # C. elegans against C. briggsae.
    ./run_inparanoid.py \
      --matrix $matrix \
      $EXTRA_ARGS \
      PRJNA13758 PRJNA10731 \
      $OUTDIR &

    # Haemonchus against C. elegans.
    ./run_inparanoid.py \
      --matrix $matrix \
      $EXTRA_ARGS \
      PRJEB506 PRJNA13758 \
      $OUTDIR &
    ./run_inparanoid.py \
      --matrix $matrix \
      $EXTRA_ARGS \
      PRJNA205202 PRJNA13758 \
      $OUTDIR &
  done
  wait
}

function run_orthomcl {
  OUTDIR=../../runs/orthomcl
  mkdir -p $OUTDIR
  EXTRA_ARGS="--only-analyze"

  ./run_orthomcl.py \
    $EXTRA_ARGS \
    PRJEB506 \
    PRJNA205202 \
    $OUTDIR &
  ./run_orthomcl.py \
    $EXTRA_ARGS \
    PRJEB506 \
    PRJNA13758 \
    $OUTDIR &
  ./run_orthomcl.py \
    $EXTRA_ARGS \
    PRJNA205202 \
    PRJNA13758 \
    $OUTDIR &
  wait

  ./compare-common-orthos.py \
    $OUTDIR/PRJEB506-PRJNA13758/results/orthomcl_results.json \
    b \
    $OUTDIR/PRJNA205202-PRJNA13758/results/orthomcl_results.json \
    b \
    | Rscript - $OUTDIR/ce_orthos_comparison.svg
}

#run_inparanoid
run_orthomcl
