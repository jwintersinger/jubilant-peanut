#!/usr/bin/env python2
'''
Filter isoforms, retaining only the longest amino acid sequence for each gene.

Usage: filter_isoforms.py <input FASTA file> <gene ID field index>

For each FASTA header, the script will split on tabs, then take the field
specified by index <gene ID field index> as the gene ID. For each distinct
gene ID, it will then output the longest corresponding sequence from amongst
the set sharing the same gene ID.
'''

import sys
from collections import defaultdict
from dregs import binf

def extract_gene_id(header, field_index):
  tokens = header.split('\t')
  return tokens[field_index]

def calculate_sequence_lengths(in_file, field_index):
  seq_lengths = defaultdict(lambda: list())

  for header, seq in binf.parse_fasta(in_file):
    gene_id = extract_gene_id(header, field_index)
    seq_lengths[gene_id].append(len(seq))

  return seq_lengths

def find_max_isoform_length_for_gene(gene_id, seq_lengths):
  return max(seq_lengths[gene_id])

def print_longest_isoforms_for_each_gene(in_file, field_index, seq_lengths):
  already_printed = []

  for header, seq in binf.parse_fasta(in_file):
    gene_id = extract_gene_id(header, field_index)
    if gene_id in already_printed:
      continue

    max_isoform_len = find_max_isoform_length_for_gene(gene_id, seq_lengths)

    if len(seq) == max_isoform_len:
      binf.write_fasta_seq(sys.stdout, header, seq)
      already_printed.append(gene_id)

def main():
  # Can't use stdin, as must read *twice* -- first to calculate length of each
  # sequence, and then to output longest sequence for each.
  input_fasta_filename = sys.argv[1]

  # Field index: index of tab-delimited field containing each sequence's gene
  # ID.  Sequences sharing the same gene ID are considered isoforms of one
  # another.
  field_index = int(sys.argv[2])

  with open(input_fasta_filename) as in_file:
    seq_lengths = calculate_sequence_lengths(in_file, field_index)
    in_file.seek(0)
    print_longest_isoforms_for_each_gene(in_file, field_index, seq_lengths)

if __name__ == '__main__':
  main()
