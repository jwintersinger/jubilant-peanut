#!/usr/bin/env python3

import argparse
import sarge
import os

class CommandManager(object):
  def __init__(self):
    self._pipelines = []

  def run(self, *args, **kwargs):
    print(args)
    p = sarge.run(*args, **kwargs)
    if 'async' in kwargs and kwargs['async']:
      self._pipelines.append(p)

  def wait(self):
    for p in self._pipelines:
      p.close()
    self._pipelines = []

CONF = {}
cm = CommandManager()

def set_config(parent_dir, run_name):
  global CONF
  CONF['BASEDIR'] = os.path.abspath(os.path.expanduser('~/work/jubilant-peanut'))
  CONF['PROTDIR'] = CONF['BASEDIR'] + '/protocols/find-annotation-orthologs'
  CONF['RUNDIR']  = os.path.abspath(os.path.join(parent_dir, run_name))
  CONF['DB_NAME'] = 'orthomcl_%s' % run_name.replace('-', '_')
  CONF['ORTHOMCL_CONFIG'] = os.path.join(CONF['RUNDIR'], 'orthomcl.config')

  # gene_id_field_index must refer to field that's identical for multiple isoforms of same gene.
  CONF['DATASETS'] = {
    'PRJEB506': {
      'proteins':   CONF['BASEDIR'] + '/data/PRJEB506/h_contortus.PRJEB506.WS239.protein.fa',
      'annotation': CONF['BASEDIR'] + '/data/PRJEB506/h_contortus.PRJEB506.WS239.annotations.gff3',
      'filtered_proteins': None,
      'filter_isoforms': True,
      'gene_id_field_index': 2,
      'friendly_name': 'MHco3',
      'orthomcl_name': 'mhco',
    },
    'PRJNA205202': {
      'proteins':   CONF['BASEDIR'] + '/data/PRJNA205202/h_contortus.PRJNA205202.WS239.protein.fa',
      'annotation': CONF['BASEDIR'] + '/data/PRJNA205202/h_contortus.PRJNA205202.WS239.annotations.gff3',
      'filtered_proteins': None,
      'filter_isoforms': True,
      'gene_id_field_index': 2,
      'friendly_name': 'McMaster',
      'orthomcl_name': 'mcma',
    },
    'PRJNA13758': {
      'proteins':   CONF['BASEDIR'] + '/data/PRJNA13758/c_elegans.PRJNA13758.WS239.protein.fa',
      'annotation': CONF['BASEDIR'] + '/data/PRJNA13758/c_elegans.PRJNA13758.WS239.annotations.gff3',
      'filtered_proteins': None,
      'filter_isoforms': True,
      'gene_id_field_index': 2,
      'friendly_name': 'C. elegans',
      'orthomcl_name': 'cael',
    },
    'PRJNA10731': {
      'proteins':   CONF['BASEDIR'] + '/data/PRJNA10731/c_briggsae.PRJNA10731.WS240.protein.fa',
      'annotation': CONF['BASEDIR'] + '/data/PRJNA10731/c_briggsae.PRJNA10731.WS240.annotations.gff3',
      'filtered_proteins': None,
      'filter_isoforms': True,
      'gene_id_field_index': 2,
      'friendly_name': 'C. briggsae',
      'orthomcl_name': 'cabr',
    },
  }

def create_dirs():
  cm.run('rm -rf ' + CONF['RUNDIR'])
  cm.run('mkdir -p ' + CONF['RUNDIR'])
  os.chdir(CONF['RUNDIR'])
  cm.run('mkdir proteins')
  cm.run('mkdir results')

def filter_isoforms(genome_id, protein_path, gene_id_field_index, regenerate_data):
  filtered_file = '{rundir}/proteins/{genome_id}.filtered_isoforms.fa'.format(
    rundir = CONF['RUNDIR'],
    genome_id = genome_id,
  )
  if regenerate_data:
    cm.run('{protdir}/filter_isoforms.py {protein_path} {gene_id_field_index} > {filtered_file}'.format(
      protdir = CONF['PROTDIR'],
      genome_id = genome_id,
      protein_path = protein_path,
      gene_id_field_index = gene_id_field_index,
      filtered_file = filtered_file,
    ))
  return filtered_file

def run_orthomcl(genomes):
  os.chdir(CONF['RUNDIR'])
  cm.run('cp -a {protdir}/orthomcl.config {orthomcl_config}'.format(
    protdir = CONF['PROTDIR'],
    orthomcl_config = CONF['ORTHOMCL_CONFIG'],
  ))
  cm.run('''sed -i 's/^dbConnectString=dbi:mysql:orthomcl:mysql_local_infile=1''' +
         '''/dbConnectString=dbi:mysql:{db_name}:mysql_local_infile=1/' {orthomcl_config}'''.format(
    db_name = CONF['DB_NAME'],
    orthomcl_config = CONF['ORTHOMCL_CONFIG'],
  ))

  cm.run('mysql -uroot -psmalldampness', input='DROP DATABASE IF EXISTS {db_name}; CREATE DATABASE {db_name};'.format(
    db_name = CONF['DB_NAME']
  ))
  cm.run('orthomclInstallSchema {orthomcl_config}'.format(
    orthomcl_config = CONF['ORTHOMCL_CONFIG'],
  ))

  cm.run('mkdir compliantFasta')
  os.chdir('compliantFasta')
  for genome in genomes:
    dataset = CONF['DATASETS'][genome]
    cm.run('orthomclAdjustFasta {orthomcl_name} {protein_path} {gene_id_idx}'.format(
      orthomcl_name = dataset['orthomcl_name'],
      protein_path = dataset['filtered_proteins'],
      # OrthoMCL is 1-based. We store this as 0-based.
      gene_id_idx = dataset['gene_id_field_index'] + 1,
    ))
  os.chdir('..')

  cm.run('orthomclFilterFasta compliantFasta/ 10 20')

  cm.run('makeblastdb -in goodProteins.fasta -dbtype prot -out goodProteins.fasta')
  cm.run('blastp ' + \
    '-db goodProteins.fasta ' + \
    '-query goodProteins.fasta ' + \
    '-out blast_results.tsv ' + \
    '-num_threads 24 ' + \

    # Below options try to replicate options recommended for use with old
    # blastall, listed at
    # https://docs.google.com/a/micand.com/document/d/1RB-SqCjBmcpNq-YbOYdFxotHGuU7RK_wqxqDAMjyP_w/pub.
    #
    # Equivalency of masking options determined via http://microbiome.wordpress.com/research/orthologs/.
    # -F "m S" -s F                      (blastall)
    #     ==
    # -seg yes -soft_masking true        (blastp)
    '-outfmt 6 ' + \
    '-max_target_seqs 100000 ' + \
    '-evalue 1e-5 ' + \
    '-seg yes ' + \
    '-soft_masking true'
  )

  cm.run('orthomclBlastParser blast_results.tsv compliantFasta/ > similarSequences.txt')
  cm.run('orthomclLoadBlast {orthomcl_config} similarSequences.txt'.format(
    orthomcl_config = CONF['ORTHOMCL_CONFIG'],
  ))

  cm.run('orthomclPairs {orthomcl_config} pairs.log cleanup=no'.format(
    orthomcl_config = CONF['ORTHOMCL_CONFIG'],
  ))
  cm.run('orthomclDumpPairsFiles {orthomcl_config}'.format(
    orthomcl_config = CONF['ORTHOMCL_CONFIG'],
  ))
  cm.run('mcl mclInput --abc -I 1.5 -o mclOutput')
  cm.run('cat mclOutput | orthomclMclToGroups {group_id_prefix} {starting_point} > groups.txt'.format(
    group_id_prefix = 'pants',
    starting_point = 1,
  ))

def process_results(genome_a, genome_b):
  os.chdir(os.path.join(CONF['RUNDIR'], 'results'))
  encoded = 'orthomcl_results.json'

  cm.run('cat {orthomcl_output} | {protdir}/parse_orthomcl.py {orthomcl_name_a} {orthomcl_name_b} > {encoded}'.format(
    orthomcl_output = '../groups.txt',
    protdir = CONF['PROTDIR'],
    orthomcl_name_a = CONF['DATASETS'][genome_a]['orthomcl_name'],
    orthomcl_name_b = CONF['DATASETS'][genome_b]['orthomcl_name'],
    encoded = encoded,
  ))

  cm.run('cat {encoded} | {protdir}/summarize_groups.py > orthologue-summary'.format(
    encoded = encoded,
    protdir = CONF['PROTDIR']
  ))

  cm.run(("cat {encoded} | {protdir}/plot_group_summary.py '{friendly_name_a}' '{friendly_name_b}' " + \
    "ortho_groups_linear.svg ortho_groups_log.svg").format(
    encoded = encoded,
    protdir = CONF['PROTDIR'],
    friendly_name_a = CONF['DATASETS'][genome_a]['friendly_name'],
    friendly_name_b = CONF['DATASETS'][genome_b]['friendly_name'],
  ))

  cm.run(('cat {encoded} | {protdir}/examine_gene_groups.py ' + \
    '{annotation_a} ' + \
    '{annotation_b} ' + \
    '> gene-groups').format(
    encoded = encoded,
    protdir = CONF['PROTDIR'],
    annotation_a = CONF['DATASETS'][genome_a]['annotation'],
    annotation_b = CONF['DATASETS'][genome_b]['annotation'],
  ))

  cm.run('{protdir}/interpret-gene-groups.sh gene-groups > gene-groups.interpretation'.format(
    protdir = CONF['PROTDIR'],
  ))

def perform_run(genome_a, genome_b, regenerate_data):
  if regenerate_data:
    run_orthomcl((genome_a, genome_b))
  process_results(genome_a, genome_b)

# This could be moved into a different function, as we only need to munge the
# FASTA files for genomes used in our current run, rather than all specified
# within CONFIG['DATASETS']. Doing so would only reduce the number of genomes
# to munge from four to three (when an outgroup is used), however, which would
# reduce the runtime by only a few minutes. Thus, I choose instead to save
# myself the effort of restructuring this operation.
def filter_all_fasta_files(datasets, regenerate_data):
  filtered = {}

  for genome_id, dataset in datasets.items():
    protein_path = dataset['proteins']
    if dataset['filter_isoforms']:
      protein_path = filter_isoforms(genome_id, protein_path, dataset['gene_id_field_index'], regenerate_data)
    dataset['filtered_proteins'] = protein_path

def generate_run_name(genome_a, genome_b):
  run_name = '{a}-{b}'.format(
    a = genome_a,
    b = genome_b,
  )
  return run_name

def main():
  parser = argparse.ArgumentParser(description='Run OrthoMCL and parse results.')
  parser.add_argument('genome_a',   help='First genome')
  parser.add_argument('genome_b',   help='Second genome')
  parser.add_argument('output_dir', help='Output directory')
  parser.add_argument('-a', '--only-analyze', dest='regenerate_data',
    action='store_false', help='Do not recreate directories or run OrthoMCL. Only analyze existing results.')
  args = parser.parse_args()

  run_name = generate_run_name(args.genome_a, args.genome_b)
  set_config(args.output_dir, run_name)
  if args.regenerate_data:
    create_dirs()
  filter_all_fasta_files(CONF['DATASETS'], args.regenerate_data)

  perform_run(args.genome_a, args.genome_b, args.regenerate_data)

if __name__ == '__main__':
  main()
