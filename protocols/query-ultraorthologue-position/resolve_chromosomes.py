#!/usr/bin/env python3
import argparse
import json
import sys

def parse_gene_mappings(gff_fn):
  gene_mappings = {}

  with open(gff_fn) as gff_file:
    for line in gff_file:
      line = line.strip()
      if line == '' or line.startswith('#'):
        continue

      fields = line.split('\t')
      if fields[2].lower() != 'gene':
        continue
      attribs = dict([f.split('=', 1) for f in fields[8].split(';')])

      if 'ID' not in attribs:
        continue
      id_tokens = attribs['ID'].split(':', 1)
      # Instead of 'gene', this may be, for example, 'gmap', indicating that
      # the gene has been mapped to an approximation position (e.g., within a
      # couple cM), but its exact position is not known. For example, see
      # spe-13 in PRJNA13758. As such genes don't appear in PRJNA13758's FASTA
      # protein file, ignore them.
      if not id_tokens[0].lower() == 'gene':
        continue

      seq_id = fields[0]
      gene_id = id_tokens[1]
      gene_mappings[gene_id] = seq_id

  return gene_mappings

def resolve_chromosomes(ultraparalogues, gene_mappings):
  for group in ultraparalogues:
    for ortho_index, ortho_params in group.items():
      gene_name = ortho_params['gene_name']
      ortho_params['seq_id'] = gene_mappings[ortho_index][gene_name]
  return ultraparalogues

def main():
  parser = argparse.ArgumentParser(description='Resolve chromosomes for ultraparalogues.')
  parser.add_argument('annotations_a')
  parser.add_argument('annotations_b')
  parser.add_argument('annotations_c')
  parser.add_argument('name_a')
  parser.add_argument('name_b')
  parser.add_argument('name_c')
  args = parser.parse_args()

  gene_mappings = {
    args.name_a: parse_gene_mappings(args.annotations_a),
    args.name_b: parse_gene_mappings(args.annotations_b),
    args.name_c: parse_gene_mappings(args.annotations_c),
  }

  ultraparalogues = json.load(sys.stdin)
  ultraparalogues = resolve_chromosomes(ultraparalogues, gene_mappings)
  print(json.dumps(ultraparalogues))

main()
