#!/usr/bin/env python3
import sys
import json
from collections import defaultdict

def organize_by_chromosome(ultraorthologues):
  reorganized = defaultdict(lambda: defaultdict(lambda: list()))
  for group in ultraorthologues:
    for genome_name, ortho_params in group.items():
      reorganized[genome_name][ortho_params['seq_id']].append(ortho_params['gene_name'])
  return reorganized

def map_hc_to_ce_orthologues(ultraparalogues, celegans_genome_name):
  hc_to_ce_map = defaultdict(lambda: {})

  for group in ultraparalogues:
    for genome_name, ortho_params in group.items():
      if genome_name == celegans_genome_name:
        continue
      hc_to_ce_map[genome_name][ortho_params['gene_name']] = group[celegans_genome_name]

  return hc_to_ce_map

def main():
  by_chromo_fname, hc_to_ce_map_fname, celegans_genome_name = sys.argv[1:]

  ultraorthologues = json.load(sys.stdin)
  uo_by_chromo = organize_by_chromosome(ultraorthologues)
  hc_to_ce_map = map_hc_to_ce_orthologues(ultraorthologues, celegans_genome_name)

  with open(by_chromo_fname, 'w') as f:
    json.dump(uo_by_chromo, f)
  with open(hc_to_ce_map_fname, 'w') as f:
    json.dump(hc_to_ce_map, f)

main()
