#!/bin/bash
set -euo pipefail

SCRIPT_DIR="$(dirname "$(readlink -f "$0")")"
BASE_DIR=~/work/jubilant-peanut
ORTHO_RESULTS_DIR=$BASE_DIR/runs/orthomcl/
OUTPUT_DIR=$BASE_DIR/runs/query-ultraorthologues-position

ULTRAPARALOGUES=$OUTPUT_DIR/ultraparalogues.json
ULTRAPARALOGUES_WITH_CHROMOSOMES=$OUTPUT_DIR/ultraparalogues_with_chromosomes.json
ULTRAPARALOGUES_BY_CHROMO=$OUTPUT_DIR/uo_by_chromo.json
ULTRAPARALOGUES_BY_NAME=$OUTPUT_DIR/uo_by_name.json
HC_TO_CE_MAP=$OUTPUT_DIR/hc_to_ce_map.json
UO_CONSERVATION=$OUTPUT_DIR/uo-conservation

DATA_DIR=$BASE_DIR/data
NAME_A=PRJEB506
NAME_B=PRJNA205202
NAME_C=PRJNA13758
ANNOTATION_A=$DATA_DIR/$NAME_A/h_contortus.${NAME_A}.WS239.annotations.gff3
ANNOTATION_B=$DATA_DIR/$NAME_B/h_contortus.${NAME_B}.WS239.annotations.gff3
ANNOTATION_C=$DATA_DIR/$NAME_C/c_elegans.${NAME_C}.WS239.annotations.gff3

ORTHO_RUN_A=$ORTHO_RESULTS_DIR/${NAME_A}-${NAME_B}/results/orthomcl_results.json
ORTHO_RUN_B=$ORTHO_RESULTS_DIR/${NAME_A}-${NAME_C}/results/orthomcl_results.json

mkdir -p $OUTPUT_DIR

$SCRIPT_DIR/find_ultraorthologues.py \
  $ORTHO_RUN_A \
  $ORTHO_RUN_B \
  aa \
  $NAME_A \
  $NAME_B \
  $NAME_C \
  > $ULTRAPARALOGUES
cat $ULTRAPARALOGUES | $SCRIPT_DIR/resolve_chromosomes.py \
  $ANNOTATION_A \
  $ANNOTATION_B \
  $ANNOTATION_C \
  $NAME_A \
  $NAME_B \
  $NAME_C \
  > $ULTRAPARALOGUES_WITH_CHROMOSOMES
cat $ULTRAPARALOGUES_WITH_CHROMOSOMES | $SCRIPT_DIR/reorganize_uo_data_structures.py \
  $ULTRAPARALOGUES_BY_CHROMO \
  $HC_TO_CE_MAP              \
  $NAME_C
cat $ULTRAPARALOGUES_BY_CHROMO | $SCRIPT_DIR/plot_uo_distribution.py $OUTPUT_DIR
$SCRIPT_DIR/calc_uo_position_conservation.py \
  $ULTRAPARALOGUES_BY_CHROMO \
  $HC_TO_CE_MAP              \
  $NAME_C                    \
  $OUTPUT_DIR                \
  > $UO_CONSERVATION
