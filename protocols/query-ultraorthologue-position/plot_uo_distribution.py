#!/usr/bin/env python2

import json
import math
import sys
import os.path
from collections import defaultdict

import matplotlib
# Force matplotlib not to use X11 backend, which produces exception when run
# over SSH.
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np

def plot_uo_dist(orthos_by_seq, genome_name, output_dir):
  vals = []
  for seq_id, orthos_on_seq in orthos_by_seq.items():
    vals.append(len(orthos_on_seq))

  # Add two since upper value of bin must be *greater* than (not equal to)
  # highest value in list for there to be a bin dedicated to it.
  bins = list(range(int(math.floor(min(vals))), int(math.ceil(max(vals))) + 2))
  plt.figure()
  plt.xticks(bins)
  counts, bins, patches = plt.hist(vals, bins=bins, log=False, facecolor='green', alpha=0.5)
  plt.yscale('symlog')

  plt.title(genome_name)
  #plt.xlabel('Number of UOs on scaffold')
  plt.ylabel('Count')

  bin_centers = 0.5 * np.diff(bins) + bins[:-1]
  for count, x in zip(counts, bin_centers):
    plt.annotate(str(int(count)), xy=(x, 0), xycoords=('data', 'axes fraction'),
      xytext=(0, -18), textcoords='offset points', va='top', ha='center')

  plt.savefig(os.path.join(output_dir, genome_name + '.png'))
  
def main():
  uo_by_chromo = json.load(sys.stdin)
  for genome_name, orthos_by_seq in uo_by_chromo.items():
    plot_uo_dist(orthos_by_seq, genome_name, sys.argv[1])

if __name__ == '__main__':
  main()
