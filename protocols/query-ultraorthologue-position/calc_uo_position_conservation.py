#!/usr/bin/env python3
import sys
import json

import math
import os
from collections import Counter

import matplotlib
# Force matplotlib not to use X11 backend, which produces exception when run
# over SSH.
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

def load_json(fname):
  with open(fname) as f:
    return json.load(f)

def calc_uo_conservation(ultraorthologues, hc_to_ce_map, overlapping_windows=True):
  '''Determine ultraorthologue conservation score for given genome relative to C. elegans.'''
  scores = []
  group_size = 3
  if overlapping_windows:
    step_size = 1
  else:
    step_size = group_size

  for seqid, uo_group in ultraorthologues.items():
    n = len(uo_group)
    # Ignore scaffolds that don't have at least three ultraorthologues (UOs).
    if n < group_size:
      continue

    for i in range(0, n - (group_size - 1), step_size):
      hc_uos = uo_group[i:(i + group_size)]
      ce_scafs = [hc_to_ce_map[hc_uo_name]['seq_id'] for hc_uo_name in hc_uos]
      scores.append(len(set(ce_scafs)))

  return scores

def plot(title, vals, output_dir):
  cnt = Counter(vals)
  fig, ax = plt.subplots()
  width = 0.8
  ind = np.arange(3)
  rects = ax.bar(ind, cnt.values(), width, color='#491b3a', edgecolor='white')
  plt.xlim(-0.3, 3.1)

  params = {'text.usetex': False, 'mathtext.fontset': 'stixsans'}
  plt.rcParams.update(params)

  ax.set_xlabel(r'Number of \textit{C. elegans} scaffolds')
  ax.set_ylabel('Count')
  ax.set_title(title)
  ax.set_xticks(ind + width/2)
  ax.set_xticklabels([str(c) for c in cnt.keys()])

  for rect in rects:
    height = rect.get_height()
    ymin, ymax = plt.ylim()
    ax.text(rect.get_x() + rect.get_width()/2, height - 0.06*ymax, str(height), ha='center', va='bottom', color='white')

  edge_color = 'black'
  sns.set_style('darkgrid')
  sns.despine()
  plt.grid(False, axis='x')
  plt.savefig(os.path.join(output_dir, '%s_uo_scores.svg' % title), bbox_inches='tight')

def calculate_genome_score(uo_scores):
  return sum(uo_scores) / len(uo_scores)

def main():
  uo_by_chromo = load_json(sys.argv[1])
  hc_to_ce_map = load_json(sys.argv[2])
  celegans_genome_name = sys.argv[3]
  output_dir = sys.argv[4]

  for genome_name, ultraorthologues in uo_by_chromo.items():
    if genome_name == celegans_genome_name:
      continue
    uo_scores = calc_uo_conservation(ultraorthologues, hc_to_ce_map[genome_name])
    plot(genome_name, uo_scores, output_dir)
    print(genome_name, calculate_genome_score(uo_scores), sep='\t')

main()
