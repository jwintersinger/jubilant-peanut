#!/usr/bin/env python3
import argparse
import json
import sys

def find_one_to_one(json_fn):
  one_to_one = []

  with open(json_fn) as json_fp:
    parsed = json.load(json_fp)

  for group in parsed['groups']:
    if not (len(group['a']) == len(group['b']) == 1):
      continue
    # For the moment, at least, discard scores and retain only names.
    one_to_one.append([
      group['a'][0][0],
      group['b'][0][0]
    ])
    
  return one_to_one

def calc_ultraorthos(orthos_a, orthos_b, common_column, name_a, name_b, name_c):
  col_indexes = {'a': 0, 'b': 1}
  orthos_a_column = col_indexes[common_column[0]]
  orthos_b_column = col_indexes[common_column[1]]
  ultraorthos = []

  for ortho_a in orthos_a:
    for ortho_b in orthos_b:
      ortho_name_a = ortho_a[orthos_a_column]
      ortho_name_b = ortho_b[orthos_b_column]
      if ortho_name_a == ortho_name_b:
        ultraorthos.append({
          name_a: {'gene_name': ortho_a[0] },
          name_b: {'gene_name': ortho_a[1] },
          name_c: {'gene_name': ortho_b[1 - orthos_b_column] },
        })

  return ultraorthos

def main():
  parser = argparse.ArgumentParser(description='Find 1:1:1 orthologues (i.e., "ultraorthologues")  between three genomes.')
  parser.add_argument('orthos_a')
  parser.add_argument('orthos_b')
  parser.add_argument('common_column', choices=['aa', 'bb', 'ab', 'ba'],
    help='''Common column shared across files. For example, if
    orthos_a=PRJEB506-PRJNA205202 and orthos_b=PRJNA13758-PRJEB506, the first
    column in the first file and second column in the second file will be
    shared (i.e., they will both refer to genes in PRJEB506). In this case, the
    argument should be "ab". Likewise, "bb", for example, would indicate that
    the second columns of both files are shared.'''
  )
  parser.add_argument('name_a')
  parser.add_argument('name_b')
  parser.add_argument('name_c')
  args = parser.parse_args()

  orthos_a = find_one_to_one(args.orthos_a)
  orthos_b = find_one_to_one(args.orthos_b)
  ultraorthos = calc_ultraorthos(
    orthos_a,
    orthos_b,
    args.common_column,
    args.name_a,
    args.name_b,
    args.name_c,
  )

  print(json.dumps(ultraorthos))

main()
